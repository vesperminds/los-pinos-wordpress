<?php tt_body_class('activities-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/activities', 'header') ?>
<?php get_translated_template_part('elements/activities', 'discover-los-pinos') ?>
<?php get_translated_template_part('elements/activities', 'discover-monteverde') ?>
<?php get_translated_template_part('elements/activities', 'shelter') ?>
<?php get_translated_template_part('elements/activities', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

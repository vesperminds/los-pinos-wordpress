<?php tt_body_class('rooms-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/rooms', 'header') ?>
<?php get_translated_template_part('elements/rooms', 'rooms') ?>
<?php get_translated_template_part('elements/rooms', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

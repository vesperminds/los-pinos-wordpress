<?php tt_body_class('books-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/books', 'header') ?>
<?php get_translated_template_part('elements/books', 'books') ?>
<?php get_translated_template_part('elements/books', 'shelter') ?>
<?php get_translated_template_part('elements/books', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

<?php defined('_GALLERY_FILTER') or define('_GALLERY_FILTER', 'reserve'); ?>

<?php if (!empty($_GET['_ajax'])): ?>

<?php get_translated_template_part('elements/gallery', 'photos') ?>

<?php else: ?>

<?php tt_body_class('reserve-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/reserve', 'header') ?>
<?php get_translated_template_part('elements/reserve', 'about') ?>
<?php get_translated_template_part('elements/gallery', 'photos') ?>
<?php get_translated_template_part('elements/reserve', 'map') ?>
<?php get_translated_template_part('elements/reserve', 'shelter') ?>
<?php get_translated_template_part('elements/reserve', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

<?php endif; ?>

$(function() {

  var addErr = function(domEl) {
    var el = $(domEl);

    el.addClass('error').on('click.formerr', function() {
      $(this).removeClass('error').off('.formerr');
    });
  }

  var validateEmail = function(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  $('body').on('click', '[lp-action="contact"]', function(ev) {
    ev.preventDefault();
    var form = $($(this).attr('lp-form')).get(0),
      thanks = $('#contact-form-thanks'),
      btn = $(this);

    if (btn.data('inprogress')) {
      return;
    }

    var errors = false;

    if (!$(form.fullname).val()) {
      addErr(form.fullname);
      errors = true;
    }

    if (!$(form.phone).val()) {
      addErr(form.phone);
      errors = true;
    }

    if (!$(form.email).val() || !validateEmail($(form.email).val())) {
      addErr(form.email);
      errors = true;
    }

    if (!$(form.comments).val()) {
      addErr(form.comments);
      errors = true;
    }

    if (errors) {
      return;
    }

    btn.data('inprogress', true);

    $.ajax({
      url: window.location.href,
      type: 'post',
      data: {
        fullname: $(form.fullname).val(),
        phone: $(form.phone).val(),
        email: $(form.email).val(),
        comments: $(form.comments).val(),
        lang: $(form.lang).val(),
        '_fh': 'contact'
      },
      dataType: 'json'
    }).then(function(response) {
      if (response.done) {
        $(form).hide();
        $(thanks).fadeIn();
      } else {
        btn.data('inprogress', false);
      }
    }).fail(function() {
      btn.data('inprogress', false);
    });

  });

});

$(function() {

  var isValidDate = function(d) {
    if (Object.prototype.toString.call(d) === "[object Date]") {
      if (!isNaN(d.getTime())) {
        return true;
      }
    }

    return false;
  }

  var addErr = function(domEl) {
    var el = $(domEl);

    el.addClass('error').on('click.formerr', function() {
      $(this).removeClass('error').off('.formerr');
    });
  }

  $('body').on('click', '[lp-action="availability"]', function(ev) {
    ev.preventDefault();
    var form = $($(this).attr('lp-form')).get(0),
      url = 'https://hotels.cloudbeds.com/reservation/Vy3xrw#';

    var now = new Date();
    now.setHours(0, 0, 0, 0);

    var checkIn = new Date($(form.checkIn_submit).val()),
      checkOut = new Date($(form.checkOut_submit).val()),
      errors = false;

    if (!isValidDate(checkIn)) {
      addErr(form.checkIn);
      errors = true;
    }

    if (!isValidDate(checkOut)) {
      addErr(form.checkOut);
      errors = true;
    }

    if (!errors && checkIn < now) {
      addErr(form.checkIn);
      errors = true;
    } else if (!errors && checkOut <= checkIn) {
      addErr(form.checkOut);
      errors = true;
    }

    if (errors) {
      return;
    }

    url += 'checkin=' + checkIn.toJSON().substr(0, 10);
    url += '&checkout=' + checkOut.toJSON().substr(0, 10);

    window.open(url);

  });

});

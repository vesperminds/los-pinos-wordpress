<?php
    $reviews = get_all_reviews();
?>
<section id="advirsor-comments">
    <div class="container">
        <?php if (count($reviews) == 1): ?>
        <div class="item">
            <p>“<?= $reviews[0]['content'] ?>”</p>
            <p class="by">
                <strong><?= $reviews[0]['author'] ?></strong><br>
                <?= $reviews[0]['location'] ?>
            </p>
        </div>
        <?php elseif (count($reviews) == 0): ?>
            <!-- NO REVIEWS -->
        <?php else: ?>
        <div class="owl-advirsor">
            <?php foreach ($reviews as $review): ?>
            <div class="item">
                <p>“<?= $review['content'] ?>”</p>
                <p class="by">
                    <strong><?= $review['author'] ?></strong><br>
                    <?= $review['location'] ?>
                </p>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <?php get_translated_template_part('elements/follow-us') ?>
    </div>
</section>

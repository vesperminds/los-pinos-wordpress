<section id="header">
    <div class="content">
        <h2>Imagine waking up, opening your window</h2>
        <p>and being blinded by nature.</p>
    </div>
</section>

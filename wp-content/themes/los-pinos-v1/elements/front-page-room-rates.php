<section id="room-detail">
    <div class="container">
        <div class="content">
            <p>
                Looking for a comfortable shelter, after a long day of<br>
                activities around the Monteverde Cloud Forest?<br><br>
                Check out our
            </p>
            <a href="<?= vp_url(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ?>" class="btn">
                Rooms &amp; Rates
            </a>
        </div>
    </div>
</section>

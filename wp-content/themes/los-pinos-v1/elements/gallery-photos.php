<?php
    $filter = defined('_GALLERY_FILTER') ? _GALLERY_FILTER : null;

    if (!$filter && isset($_GET['filter'])) {
        $filter = addslashes($_GET['filter']);
    }

    $ppp = 12;
    $page = isset($_GET['_p']) ? (int) $_GET['_p'] : 1;
    if ($page < 1) {
        $page = 1;
    }

    $opts = [
        'limit' => $ppp,
        'page' => $page
    ];

    if ($filter) {
        $opts['taxonomy'] = 'photo_types';
        $opts['taxonomyslug'] = $filter;
    }

    $photos = get_all_photos($opts);
?>
<?php if (!empty($_GET['_ajax'])): ?>
<span v-more="<?= count($photos) >= $ppp ?>" style="display: none;"></span>
<?php foreach ($photos as $photo):
    if (empty($photo['extraimage'])) continue; ?>
<a class="fancybox-gallery" href="<?= $photo['extraimage'] ?>" title="<?= $photo['title'] ?>">
    <img src="<?= $photo['thumb'] ?>" alt="" />
</a>
<?php endforeach; ?>

<?php else: ?>

<section id="galery">
    <div class="container">
        <?php if (defined('_GALLERY_FILTER') && $filter == 'reserve'): ?>
        <h2><?= __('Gallery', 'p') ?></h2>
        <p><?= __('Check out some photos and videos from our reserve.', 'p') ?></p>
        <?php else: ?>
        <?php
            $types = get_terms('photo_types', [
                'hide_empty' => true
            ]);
        ?>
        <p>
            <?= __('Every image has a story to tell!', 'p') ?><br>
            <?= __('Simply click on the thumbnail to preview a slide gallery of the images and navigate through them.', 'p') ?>
        </p>
        <p>
            <?php
                $ajaxUrl = get_the_permalink();
                $ajaxUrlSep = strpos($ajaxUrl, '?') !== false ? '&' : '?';
            ?>
            <label for="filter"><?= _x('View:', 'photo type filter', 'p') ?></label>
            <span class="select-filter">
                <select id="filter" class="select-filter" data-select-link>
                    <option value="<?= $ajaxUrl ?>"><?= _x('All photos', 'all photo types', 'p') ?></option>
                    <?php foreach ($types as $type): ?>
                    <option
                        value="<?= $ajaxUrl . $ajaxUrlSep . 'filter=' . urlencode(stripslashes($type->slug)) ?>"
                        <?= $filter == $type->slug ? 'selected' : '' ?>
                    ><?= $type->name ?></option>
                    <?php endforeach; ?>
                </select>
            </span>
        </p>
        <?php endif; ?>

        <div class="galery-list grid column-4">
            <?php foreach ($photos as $photo): ?>
            <a class="fancybox-gallery" href="<?= $photo['extraimage'] ?>" title="<?= $photo['title'] ?>">
                <img src="<?= $photo['thumb'] ?>" alt="" />
            </a>
            <?php endforeach; ?>
            <div id="gallery-load-more-marker" style="display: none;"></div>
        </div>

        <?php if (count($photos) >= $ppp): ?>
        <?php
            $ajaxUrl = get_the_permalink();

            if ($filter && !defined('_GALLERY_FILTER')) {
                $ajaxUrl .= strpos($ajaxUrl, '?') !== false ? '&' : '?';
                $ajaxUrl .= 'filter=' . urlencode(stripslashes($filter));
            }
        ?>
        <a href="#" v-action="gallery-load-more" v-before="#gallery-load-more-marker" v-data="<?= tt_json_attr([ 'url' => $ajaxUrl ]) ?>" class="btn black nonwidth"><?= __('Load more photos', 'p') ?></a>
        <?php endif; ?>
    </div>
</section>

<?php endif; ?>

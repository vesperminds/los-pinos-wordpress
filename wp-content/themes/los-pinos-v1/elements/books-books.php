<?php
$books = get_all_books([
    'content' => true
]);
?>
<section id="books">
    <div class="container">
        <h2><?= __('Our Books', 'p') ?></h2>
        <ul class="book-list grid column-2">
            <?php foreach ($books as $book): ?>
            <?php
                $bookURI = '#';

                if (!empty($book['meta']['location'][0])) {
                    $bookURI = esc_url(vp_url("wp-content/{$book['meta']['location'][0]}"));
                }
            ?>
            <li class="grid column-2">
                <img src="<?= $book['thumb'] ?>" alt="">
                <div>
                    <h3><a href="<?= $bookURI ?>"><?= $book['title'] ?></a></h3>
                    <p><?= $book['content'] ?></p>
                    <p>
                        <a class="dl" href="<?= $bookURI ?>"><?= __('Download', 'p') ?></a>
                    </p>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>

<section id="welcome">
    <div class="container">
        <h2>About us</h2>
        <p>Los Pinos is a “Tico” (Costa Rican) family company that has been here in Monteverde since its origins. We are dedicated to the sustainable lodging of couples or families, in fully equipped cabins; we also protect our private reserve and the forest by calculating and reducing our ecological footprint. We participate on and support local development initiatives for the Monteverde community, as this is the environment that we live on.</p>
        <p>Over the past 26 years, we have learned from our guests, that a high quality and an authentic service, is the best form of recommendation, for which we thrive to fully accomplish these aspects.</p>
        <p>We, the Los Pinos Family, believe in the healthy interaction between the visitor and the environment, so that they can both coexist, without damaging the natural equilibrium that allows Monteverde, to be the most visited Cloud Forest in the World.</p>

		<h2>Mission</h2>
		<p>Offer guests a quality accommodation in communion with nature, which provides a local family service thatempathizes with our clients and projects to our community.</p>

		<h2>Vission</h2>
		<p>Be in 2020 the best accommodation in Monteverde, which through an integral management, serves our guests, staff and community.</p>

		<h2>Values</h2>
		<ul>
			<li>Local Family Service</li>
			<li>Empathy</li>
			<li>Community Projection</li>
			<li>Environmental Commitment</li>
		</ul>

		<h2>Environmental Policy</h2>
		<p>Los Pinos – Cabañas & Jardines is committed to providing services in the field of accommodation, ensuring its customers the quality, safety and effectiveness in them, while integrating the following:</p>
		<ul>
			<li><b>Pollution Prevention:</b> Plans will be implemented to ensure efficient management of water, energy and resource waste, so that any contamination to natural resources is prevented.</li>
			<li><b>Environmental Legislation Compliance:</b> will be given as determined by the legal authorities and other relevant agencies with which commitments are voluntarily established.</li>
			<li><b>Acquisitions and environmentally responsible contracts:</b> The procurement and contracting process will include environmental criteria to ensure the acquisition of products and services whose environmental impact is brought to a minimum.</li>
			<li><b>Social responsibility:</b> Plans and actions of community development will be applied, environmental impact control, environmental conservation and sustainability in the use of resources in the surrounding communities will be implemented by allocating the necessary resource and contributing to improve the quality of life of the habitants.</li>
		</ul>
		<p>This commitment is extended to our staff and all the activities that we run, always pointing to leadership, innovation and continuous improvement in our mission.</p>

		<h2>No Tolerance Policy to Sexual Exploitation of Youth (ESCNNA)</h2>
		<p>Los Pinos – Cabañas & Jardines is committed to a culture of "zero tolerance" against all forms of commercial sexual exploitation of children and adolescents (ESCNNA, for its acronym in Spanish) to modify cultural patterns that justify authoritarianism, discretion, the arbitrariness of adults with children and adolescents. Focusing our actions to make significant contributions so that Monteverde and Costa Rica, become places where ESCNNA is non-existent. The integrity of every person should be sacred, especially when it comes to minors, in defense of their rights, supporting this struggle with the initiative known as the Code of Conduct.</p>

        <a href="<?= vpth_path('/downloads/map.pdf') ?>" target="_blank" class="btn bordergreen">Download this map and find us in Monteverde</a>
    </div>
</section>

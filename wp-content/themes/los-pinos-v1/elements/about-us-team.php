<?php
    $team_members = get_all_team_members([
        'content' => true,
        'meta' => true
    ]);
?>
<section id="our-team">
    <div class="container">
        <h2><?= __('Our Team', 'p') ?></h2>
        <p><?= __('Each and every one of our collaborators represent our spirit. Our success is based on our unique culture and work atmosphere. We are passionate and proud, we care for the environment, the well-being of our collaborators and community, and are focused on exceeding our guest’s expectations at all times.', 'p') ?></p>
        <br>
        <div class="owl-team">
            <?php foreach ($team_members as $team_member): ?>
            <div class="item">
                <img src="<?= $team_member['thumb'] ?>" alt="">
                <p class="by">
                    <strong><?= $team_member['title'] ?></strong><br>
                    <?= isset($team_member['meta']['role'][0]) ? $team_member['meta']['role'][0] : '' ?>
                </p>
                <p>
                    <?= strip_tags($team_member['content']) ?>
                </p>
            </div>
            <?php endforeach; ?>
        </div>
    </div>

</section>

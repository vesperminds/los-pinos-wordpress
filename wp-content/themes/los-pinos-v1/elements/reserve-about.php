<section id="about-reserve">
    <div class="container">
        <h2>Private Reserve Los Pinos</h2>
        <p>We are part of the Bellbird Corridor that connects the cloud forest with the mangroves in the pacific coast.</p>
        <p>To attract more wildlife to our property, in the last couple years we have been replacing exotic plants and ornamental plants grown in greenhouses, for native plants that also require less care and less irrigation during the summer. We can now observe more wildlife than we used to, and can regularly spot birds and mammals throughout the whole year, all you need is some binoculars and you’re ready for a short hike around our trails.</p>
        <p>40% of our 7.2 hectare (18 acre) property is dedicated to the commercial renting of the cabanas and vegetable production and the other 60% is dedicated to conservation and reforestation. We are part of the Bellbird Corridor that connects the cloud forest with the mangroves in the pacific coast.</p>

        <ul class="reserve-icons grid column-5">
            <li>
                <img src="<?= vpth_path('/img/about-reserve/01.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/02.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/03.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/04.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/05.jpg') ?>" alt="">
            </li>
        </ul>
    </div>
</section>

<section id="sustainability-sliders">

    <div class="container">
        <h2>Local Initiatives</h2>
        <div class="owl-sustaniability-01">
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Committee of Integral Waste Management (COMIRES)</strong><br>
                        </p>
                        <p>Sub-entity of the Municipal Council, responsible for the planning of waste management in the community of Monteverde.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Safety Commission</strong><br>
                        </p>
                        <p>Commission that responds to the strategy of the Municipal Council of Monteverde includes the participation of the Tourist Police, Traffic Police, Investigation Organism and the Rural Guard.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Monteverde Brilla Festival Commission</strong><br>
                        </p>
                        <p>Festival Commission: Committee of volunteers from the community, with the participation of the Municipal Council, responsible for the planning and execution of the Monteverde Brilla Festival, meetings are every first Thursday of December.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Monteverde Road Link Committee</strong><br>
                        </p>
                        <p>Committee representing the community of Monteverde before State institutions, in topics related to the construction of the route 606.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Network ProNativas:</strong><br>
                        </p>
                        <p>Organization that educates people about the importance, reproduction and use of native ornamental plants. <a href="http://www.pronativas.org" target="_blank">www.pronativas.org</a></p>
                    </div>
                </div>
            </div>
            <!-- <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>CBPC</strong><br>
                        </p>
                        <p>{{TODO}}</p>
                    </div>
                </div>
            </div> -->
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Monteverde Conservation League (MCL):</strong><br>
                        </p>
                        <p>Civil, nonprofit organization that seeks to conserve, preserve and rehabilitate tropical ecosystems and their biodiversity. It develops projects of social, environmental and economic importance. <a href="http://www.acmcr.org" target="_blank">www.acmcr.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Monteverde Community Fund (MCF):</strong><br>
                        </p>
                        <p>Philanthropic organization that connects resources and strategic actors in the community to boost Monteverde sustainability initiatives and their surrounding communities. <a href="http://www.monteverdefund.org" target="_blank">www.monteverdefund.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Chamber of Tourism Entrepreneurs from Monteverde:</strong><br>
                        </p>
                        <p>Local entity that unifies and organizes tourism enterprises in the development of the community.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2>National Initiatives</h2>
        <div class="owl-sustaniability-01">
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>National Chamber of Ecotourism and Sustainable Tourism (CANAECO)</strong><br>
                        </p>
                        <p>Non-governmental, non-profit, and private character organization. Promotes the development of the tourist activity in the country in a responsible and sustainable manner. <a href="http://www.canaeco.org" target="_blank">www.canaeco.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>National Chamber of Tourism</strong><br>
                        </p>
                        <p>Non-profit organization, which brings together the chambers and associations of the tourist business in the country. <a href="http://www.canatur.org" target="_blank">www.canatur.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Network of Private Reserves</strong><br>
                        </p>
                        <p>Organization that groups the private reserves of Costa Rica, encouraging voluntary conservation. <a href="http://www.reservasprivadascr.org">www.reservasprivadascr.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Costa Rica SMEs seal (PYME)</strong><br>
                        </p>
                        <p>Seal that identifies small and medium-sized companies that comply with their tax obligations, social charges of the CCSS, working risks policy. <a href="http://www.pyme.go.cr" target="_blank">www.pyme.go.cr</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Code of Conduct</strong><br>
                        </p>
                        <p>It is a commitment that has acquired Costa Rican tourism industry to discourage and punish Commercial Sexual Exploitation of Children and Adolescents (CSECA), as a hub of responsible and sustainable tourism. <a href="http://www.paniamor.org" target="_blank">www.paniamor.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Ecological Blue Flag Program</strong><br>
                        </p>
                        <p>Annually given award, it rewards effort and volunteer work in the search for conservation and development, in accordance with the protection of natural resources, the implementation of actions to address climate change, the pursuit of improved hygienic sanitation and improvement of public health. <a href="http://www.banderaazulecologica.org" target="_blank">www.banderaazulecologica.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Green Flag Program or Sanitary Quality Seal</strong><br>
                        </p>
                        <p>Award granted by the Costa Rican Institute of Aqueducts and Sewers (AyA). It aims to promote the participation of civil society to improve the operation and maintenance of the aqueducts in the country.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Certification for Sustainable Tourism</strong><br>
                        </p>
                        <p>Certification of the Costa Rican Tourism Institute (ICT), designed to differentiate tourism businesses according to the degree in which its operation and functioning is approaching a model of sustainability. <a href="http://www.turismo-sostenible.co.cr" target="_blank">www.turismo-sostenible.co.cr</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h2>Costa Rican Environmental Regulations</h2>
        <div class="owl-sustaniability-01">

            <div class="item">
                <p class="by">
                    <strong>Law about Archaeological Heritage (Law N° 6703)</strong><br>
                </p>
                <p>At Los Pinos-Cabañas &amp; Jardines we recognize the importance of our national archaeological heritage, for which we convincingly defend this valuable resource. We are committed to the protection of all archaeological cultural goods defined by the Law N°6703, as all the products of the activities of the human groups of the past (specifically, indigenous prior or contemporaneous to the establishment of the hispanic culture in national territory). To this end we proceed through the following actions in any situation that follow against the national archaeological heritage:</p>

                <p>&bull; Training of our collaborators on the law No. 6703 of national archaeological heritage.</p>
                <p>&bull; Communication of the law.</p>
                <p>&bull; Formal demand to the National Museum in case of any situation that threatens the heritage.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Organic Environmental Law (Law N° 7554)</strong><br>
                </p>
                <p>In compliance with the provisions of the LOA, we follow carry our development in harmony with nature, ensuring that each of our activities are carried out in a responsible and respectful way. We work towards minimum contamination of: soil, air, rivers, wilderness areas, forests, and any other considered within this law. Within our operation we develop these aspects through Programs of Integral Management of Wastes, Management of Water and Energy Resources, as well as a Corporate Social Responsibility Program in which we include environmental education.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Biodiversity Law (Law N° 7778)</strong><br>
                </p>
                <p>We are committed to the conservation of biodiversity and the sustainable use of resources; we have made these priorities in our operation. Our property was registered as a Private Reserve since 2011. We have implemented a Program of Monitoring of Species as well as measures for the sustainable use of our resources. We are also part of the Bell Bird Biological Corridor, through which we work as community and region for the benefit of the connectivity of mountain ecosystems of Monteverde and the mangroves on the coast in Puntarenas.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Law Against Child Commercial Sexual Exploitation (Law N° 7899)</strong><br>
                </p>
                <p>At Los Pinos-Cabañas &amp;Jardines we work in defense of the integrity and rights of the Costa Rican habitants, in general adults, adolescents, and children. We develop our tourism activities under the strict levels of accountability and monitoring, always alert to report any activity of Commercial Sexual Exploitation of Children and Adolescents (CSEC). Our staff has been qualified and trained in this issue; we are committed with the dissemination of this information to all of our public of interest.</p>
                <p>We work in a culture of "zero tolerance" against all forms of exploitation of children and adolescents, so we also participate in the national initiative of the tourism sector in the Code of Conduct and encourage other enterprises of our city, region and country to form part of the same.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Law on Narcotics, Psychotropic Substances and Drugs of not Authorized Use (Law N° 8204)</strong><br>
                </p>
                <p>In the property of Los Pinos-Cabañas &amp; Jardines is strictly prohibited any type of narcotic treatment or drug-related trade, and/or unauthorized use psychotropic substances. In Costa Rica, this type of action is penalized with prison. If any similar fact will take respective measures in terms of complaint (in case of customers or suppliers involved) or dismissal (in the case of officials involved), this according to our corresponding claim protocol.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Law against Sexual harassment in employment and teaching (Law N° 7476)</strong><br>
                </p>
                <p>At Los Pinos-Cabañas &amp;Jardines we do not allow any the staff members incurred in a situation of sexual-labor harassment, if so he or she will be dismissed immediately as established by this law, without employer liability and/or penalties in terms of payments for compensation to the victim; depending in the case it could include prison. This company will severely punish any hint of sexual harassment towards anyone or any situation in which any of our staff members is involved during labor time inside or outside the company's facilities.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <h2>Regulations for Visits to Protected Areas</h2>
        <div class="owl-sustaniability-01">

            <div class="item">
                <p class="by">
                    In Costa Rica, since 1991 regulations for public use for protected areas that receive visitors in greater numbers have been published trough Executive Decrees. For each site, activities and areas restrictions have been defined, visitation and observation of species are regulated, non-permitted activities and prohibitions are defined. Is of outmost importance that all visitors meet these regulations, you can find official information in the National System of Conservation Areas (SINAC)
                </p>
            </div>

            <div class="item">
                <p class="by">
                    It is essential that tourists will cooperate with an adequate disposal of solid waste-generating, separating them and putting them in containers and places allocated for this purpose.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    All visitations to protected areas must be done with a conservationist mentality and attitude of protection of the environment and defense of the wildlife rescue.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    The property of Los Pinos-Cabañas &amp;Jardines was registered as a Private Reserve since the year 2011. Ensuring cleaner air in our property we have declared this as a tobacco-free space, we expect no smoking inside our property. This restriction sets as a regulation measure that applies, for staff, clients and visitors in general.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    In case of taking food to natural areas, do not feed the animals or leave in these sites remains of them, to avoid any impact generated in the local fauna.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Avoid, to the maximum, all kinds of unnecessary disturbance to wildlife, as well as the same habitat. The noise should be minimized by walking into the paths, keeping their distance from the species and not touching the plants.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Respect the demarcation and always walk within the marked trails, avoiding walking out of them.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    If you find another group of visitors as you walk into the pathways, please maintain a reasonable distance between both groups, this to avoid the alteration of the excess capacity allowed.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    The tourist areas located near Los Pinos-Cabañas &amp;Jardines offer large commercial, cultural, and tourist attractions, we recommend to visit the surrounding communities. This way you will also support the settlers, through the acquisition of goods and services in the town, motivating them to a positive community interaction and working to support the conservation activity.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Do not buy souvenirs or memories of your trip, or artisanal, manufactured articles made with wild species, woods in danger of extinction, this with the purpose to avoid being sanctioned by the local authorities. Our law system prohibits and punishes with heavy fines, the acquisition, marketing or possession of articles of this type.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    In Costa Rica we are defenders of children and adolescents which may be threatened by the commercial sexual exploitation of children. If you see a situation that suggests any indication of this despicable activity, let us know to proceed immediately according to the procedure of the code of international conduct, entity to which we have allied many tourism companies to fight together against such action. In Costa Rica this activity is punished with imprisonment without the possibility of bail to a minimum of 8 years.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    As to the previous point, it is important to take into account that we count on a law against the consumption of illegal substances (possession, consumption, and trade) and a law on sexual harassment. Los Pinos-Cabañas &amp;Jardines, we have trained our staff on these issues, providing appropriate guidance and establishing an internal regulation that ensures an environment of respect and harmony for all.
                </p>
            </div>
        </div>
    </div>
</section>

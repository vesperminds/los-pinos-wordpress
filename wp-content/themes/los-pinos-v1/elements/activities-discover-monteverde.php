<section class="discovery">
    <div class="container">
        <h2>Discover the great activities that Monteverde can offer to you.</h2>
    </div>

    <div class="discovery-list discovery-monteverde grid column-4">
        <a href="#" data-id="0" class="active">
            <img src="<?= vpth_path('/img/discovery/01_b.jpg') ?>" alt="">
            <p>Monteverde Cloud Forest<br>Biological Reserve</p>
        </a>
        <a href="#" data-id="1">
            <img src="<?= vpth_path('/img/discovery/02_b.jpg') ?>" alt="">
            <p>Santa Elena<br>Reserve</p>
        </a>
        <a href="#" data-id="2">
            <img src="<?= vpth_path('/img/discovery/03_b.jpg') ?>" alt="">
            <p>Hanging<br>Bridges</p>
        </a>
        <a href="#" data-id="3">
            <img src="<?= vpth_path('/img/discovery/04_b.jpg') ?>" alt="">
            <p>Canopy<br>Tour</p>
        </a>
    </div>

    <div class="container">
        <div class="discovery-item active">
            <p>Private non-profit reserve (not a national park) administered by the Tropical Science Center. There are 6 distinct ecological zones in this reserve. It is called a cloud forest rather than a rain forest because of its altitude: the clouds go thru the forest. The canopy is extremely rich with birds, insects, butterfly, and thousands of plants. It possesses 100 species of mammals, 400 of birds, 120 of amphibians and reptilian, 2500 of plants (500 are orchids) and dozens of thousands of insects. Great bird watching, as the Three-Wattled Bellbird and Resplendent Quetzal is usually seen in the March-May nesting season. Bring a warm jacket, raingear (a green poncho is just fine) and footwear for trail use. Rubber boots are usually not necessary due to the well-maintained trails. Open to the public daily from 07: 00 to 16:00. Adults US $13, students with ID and child $ 6.5 Natural history walks US$15 + entrance fee.</p>
        </div>
        <div class="discovery-item">
            <p>The 765-acre Santa Elena Reserve is located 6 kilometers northeast of Santa Elena. It is at a slightly higher elevation than the Monteverde reserve and tends to be cloudier and wetter, but is also less visited than its better know neighbor. There are over 10 kilometers of trails, with one leading to a lookout point from which you can see Volcán Arenal on a clear day. There are usually local guides on hand, or you can hire one before hand through your hotel. You have the option of buying a self-guided trail map and renting boots at the entrance. The reserve was created out of a community's determination to help preserve the unique cloud forest surrounding them and to use tourism as a tool to benefit community development in Monteverde. Entrance fees are used for the protection and management of the Reserve and to provide higher quality education for schools of Monteverde. Open daily 07:00 to 16:00. Entrance US $8.00, students with ID $3.50.</p>
        </div>
        <div class="discovery-item">
            <p>This new attraction offers an opportunity to explore the forest canopy in a safe and easy manner, walking a simple trail system through the tropical jungle featuring eight suspension bridges that crisscross deep canyons at the level of the tree tops. The longest bridge is some 170 meters (558 feet) in length. Open to the public daily from 07:00 to 16:00. Admission: US $ 20 adults, students with ID $15 and children (up 3 years) US $10.Guided hikes are available with advanced reservations. Duration: 1:30 hours. From our reception tours leaves daily at 07:45, 10:15, 12:15 and 13:45.</p>
        </div>
        <div class="discovery-item">
            <p>If you're looking for some adventure, try this canopy expedition that takes you from tree top platform to tree top platform via a series of zip line cables while enjoying the beauty of the cloud forest canopy. Selvatura Canopy Tour is one of the safest and most thrilling adventures of all time. In this tour the visitor will be able to see the marvels of the virgin cloud forest from one canopy perspective. It is also possible to observe same of the cloud forests most amazing flora and fauna including a wide variety of epiphytes, monkeys and exotic birds. Surely this tour is the experience of a lifetime. From our reception tours leaves daily at 07:45, 10:15, 12:15 and 13:45. Group size is limited to 15 persons, including the guide. Tour duration: 3 hours. US $ 40 adults, students with ID US $30 and children (up 3 years) US $ 25.</p>
        </div>
    </div>
    <div class="container">
         <a href="<?= vpth_path('/books/monteverde.pdf') ?>" class="btn bordergreen" target="_blank">DOWNLOAD MONTEVERDE ACTIVITIES</a>
    </div>
</section>

<section id="main-map">
    <div class="container">
        <h2>Map</h2>
        <p>
            Are you planning to visit us?<br>
            Please check out the Reserve Los Pinos Map.
        </p>
        <br><br>
        <img class="fullwidth" src="<?= vpth_path('/img/about-reserve/map.jpg') ?>" alt="">
    </div>
</section>

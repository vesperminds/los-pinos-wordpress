<section id="welcome">
    <div class="container">

        <form action="" id="contact-form">
            <h2>Contact Us</h2>
            <p>Please fill the following information. All the fields are required.</p>

            <fieldset>
                <label for="">Full Name*</label>
                <input type="text" name="fullname" required>
            </fieldset>

            <fieldset>
                <label for="">Phone Number*</label>
                <input type="text" name="phone" required>
            </fieldset>

            <fieldset>
                <label for="">Email*</label>
                <input type="email" name="email" required>
            </fieldset>

            <fieldset>
                <label for="">Comments*</label>
                <textarea name="comments" required></textarea>
            </fieldset>

            <p>* All fields are required</p>
            <input type="hidden" name="lang" value="<?= __('English', 'p') ?>">
            <a href="#" lp-action="contact" lp-form="#contact-form" class="btn greenborder nonwidth">Send now</a>
        </form>

        <div id="contact-form-thanks" style="display: none;">
            <h2>Thank you!</h2>
            <p>
                We're going to get in touch with you
                as soon as possible.
            </p>
        </div>
    </div>
</section>

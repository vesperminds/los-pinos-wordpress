<section id="header">
    <div class="content">
        <h2>We believe in the healthy interaction</h2>
        <p>Between the visitor and the environment.</p>
    </div>
</section>

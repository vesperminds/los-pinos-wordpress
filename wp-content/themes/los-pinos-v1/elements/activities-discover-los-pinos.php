<section class="discovery">
    <div class="container">
        <h2>Discover the great activities that Los Pinos can offer to you</h2>
    </div>

    <div class="discovery-list discovery-lospinos grid column-4">
        <a href="#" data-id="0" class="active">
            <img src="<?= vpth_path('/img/discovery/01_a.jpg') ?>" alt="">
            <p>Hydroponic<br>Garden</p>
        </a>
        <a href="#" data-id="1">
            <img src="<?= vpth_path('/img/discovery/02_a.jpg') ?>" alt="">
            <p>Trails<br> </p>
        </a>
        <a href="#" data-id="2">
            <img src="<?= vpth_path('/img/discovery/03_a.jpg') ?>" alt="">
            <p>Kids<br>Playground</p>
        </a>
        <a href="#" data-id="3">
            <img src="<?= vpth_path('/img/discovery/04_a.jpg') ?>" alt="">
            <p>View<br>Seeing</p>
        </a>
    </div>

    <div class="container">
        <div class="discovery-item active">
            <p>Hydroponics is proved to have several advantages over soil gardening. The growth rate on a hydroponic plant is 30-50 percent faster than a soil plant, grown under the same conditions. The yield of the plant is also greater. Scientists believe that there are several reasons for the drastic differences between hydroponic and soil plants.</p>

        </div>
        <div class="discovery-item">
            <p>While walking the trails, visitors may witness the contrasting panoramas exhibited naturally by the continental divide, a geographical feature that traces the principal mountain ranges of Costa Rica.</p>

        </div>
        <div class="discovery-item">
            <p>A playground, playpark, or play area is a place with a specific design to allow children to play there. It may be indoors but is typically outdoors (where it may be called a tot lot in some regions). While a playground is usually designed for children, some playgrounds are designed for other age groups</p>

        </div>
        <div class="discovery-item">
            <p>Contrasting panoramas exhibited naturally by the continental divide, a geographical feature that traces the principal mountain ranges of Costa Rica.</p>
            
        </div>
    </div>
</section>

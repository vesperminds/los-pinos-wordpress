<section id="welcome">
    <div class="container">

        <form action="" id="contact-form">
            <h2>Contáctenos</h2>
            <p>Por favor llene la siguiente información. Todos los campos son requeridos.</p>

            <fieldset>
                <label for="">Nombre completo*</label>
                <input type="text" name="fullname" required>
            </fieldset>

            <fieldset>
                <label for="">Número de teléfono*</label>
                <input type="text" name="phone" required>
            </fieldset>

            <fieldset>
                <label for="">Correo electrónico*</label>
                <input type="email" name="email" required>
            </fieldset>

            <fieldset>
                <label for="">Comentarios*</label>
                <textarea name="comments" required></textarea>
            </fieldset>

            <p>* Todos los campos son requeridos</p>
            <input type="hidden" name="lang" value="<?= __('English', 'p') ?>">
            <a href="#" lp-action="contact" lp-form="#contact-form" class="btn greenborder nonwidth">Enviar</a>
        </form>

        <div id="contact-form-thanks" style="display: none;">
            <h2>¡Gracias!</h2>
            <p>
                Nos pondremos en contacto contigo
                a la brevedad.
            </p>
        </div>
    </div>
</section>

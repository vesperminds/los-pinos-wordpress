<section id="welcome">
    <div class="container">
        <h2>Acerca de nosotros</h2>
        <p>Los Pinos es una empresa familiar de “Ticos” que hemos vivido en Monteverde desde sus orígenes. Nos dedicamos al alojamiento sostenible en cabañas acondicionadas para parejas y/o familias, además preservamos nuestra reserva y el bosque mediante la medición y reducción de nuestra huella ecológica. Nos gusta apoyar las iniciativas de desarrollo local para la comunidad de Monteverde ya que es nuestro entorno.</p>
        <p>Hemos recorrido 26 años aprendiendo de nuestros clientes que el servicio auténtico y de calidad es la mejor carta de presentación.</p>
        <p>En la Familia de Los Pinos creemos en la sana interacción entre el visitante y el entorno, para que ambos coexistan, sin afectar los equilibrios naturales que permiten a Monteverde seguir siendo el Bosque Nuboso más visitado del mundo.</p>

        <h2>Misón</h2>
        <p>Ofrecer al visitante un alojamiento de calidad en comunión con la naturaleza, brindando un servicio familiar autóctono, de empatía con el cliente y con proyección a la comunidad.</p>

        <h2>Visión</h2>
        <p>Llegar a ser en el 2020 el mejor alojamiento de Monteverde, que con una gestión integral sirva a sus clientes, sus colaboradores y a su comunidad.</p>

        <h2>Valores</h2>
		<ul>
			<li>Servicio Familiar Autóctono</li>
			<li>Empatía</li>
			<li>Proyección a la Comunidad</li>
			<li>Compromiso Ambiental</li>
		</ul>

		<h2>Política Ambiental</h2>
		<p>Los Pinos - Cabañas & Jardines se compromete a brindar servicios en el campo del alojamiento, asegurando a sus clientes la calidad, seguridad y efectividad en los mismos, a la vez que  vela por la integración de los siguientes aspectos:</p>
		<ul>
			<li><b>Prevención de la contaminación:</b> Se implementarán planes de gestión para asegurar el manejo eficiente del recurso hídrico, energético y residuos, de modo que se prevenga toda contaminación a los recursos naturales.</li>
			<li><b>Cumplimiento de la legislación ambiental:</b> Se dará cumplimiento a lo determinado por las autoridades legales y otros organismos competentes con los cuales se establezcan voluntariamente compromisos de tipo legal.</li>
			<li><b>Adquisiciones y contrataciones ambientalmente responsables:</b> El proceso de compras y contrataciones incluirán criterios ambientales que aseguren la adquisición de productos y servicios cuyo impacto ambiental sea llevado al mínimo.</li>
			<li><b>Responsabilidad social:</b> Se efectuarán planes y se implementarán acciones de desarrollo comunal, control de impactos ambientales, conservación ambiental y sostenibilidad en el uso de los recursos en las comunidades aledañas, asignando el recurso necesario y coadyuvando en la mejora de la calidad de vida de sus habitantes.</li>
		</ul>
		<p>Este compromiso es extendido a nuestro personal y todas las actividades que el mismo ejecute, apuntando siempre al liderazgo, innovación y mejora continua en nuestro cometido.</p>

		<h2>Política de No Tolerancia a la ESCNNA</h2>
		<p>Los Pinos - Cabañas & Jardines se compromete con una cultura de “cero tolerancia” contra todaslas formas de explotación sexual comercial de niños, niñas y adolescentes (ESCNNA), para así modificar los arraigados patrones culturales que justifican el autoritarismo, la discrecionalidad, la arbitrariedad que tienen los adultos con las niñas, niños y adolescentes. Enfocando nuestras acciones en aportar contribuciones significativas para que Monteverde y Costa Rica, se conviertan en lugares donde la ESCNNA acabe. La integridad de toda persona debe de ser sagrada, más aún cuando se trata de menores de edad, se velará en defensa de sus derechos, respaldando esta lucha por la iniciativa conocida como el Código de Conducta.</p>

        <a href="<?= vpth_path('/downloads/map.pdf') ?>" target="_blank" class="btn bordergreen">Descarga este mapa y encuéntranos en Monteverde</a>
    </div>
</section>

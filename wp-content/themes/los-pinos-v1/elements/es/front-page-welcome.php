<section id="welcome">
    <div class="container">
        <h2>Bienvenido a Los Pinos - Cabañas &amp; Jardines</h2>
        <p>Establecido en 1987, nuestro alojamiento familiar y altamente sostenible, es ahora una referencia en la zona.</p>
        <p>Disfrute de nuestras cabañas acondicionadas, inmersas en la naturaleza, rodeadas de bellos jardines nativos y bosques exuberantes, con amplio espacio entre sí y equipadas con todo lo esencial para cocinar en ellas. Explore nuestras 8 hectáreas de reserva privada o ¡aprecie la biodiversidad desde su ventana! Todo mientras se aloja cerca de todas las actividades que Monteverde tiene para ofrecer.</p>
        <p>Recorra nuestros senderos privados que le llevarán hasta nuestro bello mirador y a un Jardín Hidropónico, donde podrá cosechar una variedad de vegetales para consumir durante su estadía.</p>
        <p>Todo lo anterior nos convierte en su mejor alternativa Sostenible en el mundialmente reconocido destino ecológico que es Monteverde. Con nosotros puede alojarse en familia o con amigos, lejos del estrés de la ciudad y todos esos lugares congestionados. Si no desea cocinar, no hay problema, hay restaurantes muy cerca, a solo 400 metros de las cabañas.</p>
        <div class="qualifications grid column-4">
            <div>
                <img src=<?= vpth_path('/img/qualification/01.jpg') ?> alt="">
            </div>
            <div>
                <img src=<?= vpth_path('/img/qualification/02.jpg') ?> alt="">
            </div>
            <div>
                <img src=<?= vpth_path('/img/qualification/03.jpg') ?> alt="">
            </div>
            <div>
                <img src=<?= vpth_path('/img/qualification/04.jpg') ?> alt="">
            </div>
        </div>
    </div>
</section>

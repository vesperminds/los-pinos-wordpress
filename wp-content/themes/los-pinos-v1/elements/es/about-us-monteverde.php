<section id="about-monteverde">
    <div class="container">
        <h2>Sobre Monteverde</h2>

        <div class="borderline grid column-2">
            <div>
                <div><strong>Temperatura promedio: </strong> <p>16-22 C (61-72 F)</p></div>
                <div><strong>Precipitación anual: </strong> <p>3.000 mm (118 inches)</p></div>
                <div><strong>Verano: </strong> <p>Diciembre a Marzo.</p></div>
                <div><strong>Located: </strong>  <p>200 Km. de San José, 3:00 horas<br>en 4x4, 4:00 horas en automóvil y<br>4:30 horas en bus.</p></div>
            </div>
            <div>
                <div>
                    <div><strong>Población de Monteverde: </strong> <p> 250</p></div>
                    <div><strong>Población de Santa Elena: </strong> <p> 6.500</p></div>
                    <div><strong>Altitud, Santa Elena: </strong> <p>1.250 metros o 4.100 pies,</p></div>
                    <div><strong>El sendero más alto: </strong>  <p>1.800 metros o 5.900 pies.</p></div>
                </div>
            </div>
        </div>

        <p>La región se ubica a unos 1500 metros msnm (4920 pies) sobre la División Continental, abarca la Reserva de Santa Elena y la Reserva Biológica de Monteverde, que ofrecen unos de los lugares más interesantes para visitar en Costa Rica.</p>
        <p>Esta área es aclamada por ser uno de los más sorprendentes refugios de vida silvestre en los trópicos del nuevo mundo y amado por los ecoturistas.</p>
        <p>En Monteverde existen más de 100 especies de mamíferos; incluidas las 5 especies de grandes felinos. Adicionalmente entre su flora y fauna pueden ser encontradas:</p>
        <p>
            - 400 especies de aves incluidas 30 especies de colibríes, el Quetzal y el Pájaro Campaña<br>
            - 500 especies de mariposas<br>
            - 120 especies de reptiles y anfibios<br>
            - 2.500 especies de plantas (entre ellas 420 diferentes tipos de orquídeas y 200 especies de helechos)<br>
            - 500 especies de árboles<br>
            - Miles de especies de insectos
        </p>
        <p>
            Entre las 400 especies de aves las más conocidas son: el Quetzal, el Pájaro Campana, la Pava Negra, el Momoto, el Ala de Sable Violáceo, el Trogón Ventrianaranjado y el Pájaro Sombrilla.
        </p>
        <p>Entre los mamíferos encontrados se destacan los Zainos Collarejos, el Venado de Montaña, el Margay, el mono Ardilla y el Aullador, el Pizote, el Kinkajou, el Olingo, la Cherenga y el Tepezcuintle, el Perezoso de Dos Dedos y el Armadillo.</p>
        <br><br>
        <h3>
            Ahora es tiempo de buscar un refugio cómodo.<br>
            Ver nuestras
        </h3>
        <br>
        <a href="<?= vp_url(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ?>" class="btn black">Habitaciones y Precios</a>
    </div>
</section>

<section id="sustainability-sliders">

    <div class="container">
        <h2>Iniciativas locales</h2>
        <div class="owl-sustaniability-01">
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Comisión de Manejo Integral de Residuos (COMIRES)</strong><br>
                        </p>
                        <p>Ente adscrito al Consejo Municipal, encargado de la planificación del Manejo de Residuos en la comunidad de Monteverde.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Corredor Biológico de Monteverde/Bellbird Biological Corridor</strong><br>
                        </p>
                        <p>El Corredor Biológico Pájaro Campana es un proceso que reúne a diversos actores que impulsan acciones para la protección de la biodiversidad, la conectividad entre los ecosistemas, el uso racional de los recursos naturales y el desarrollo solidario y equitativo de las comunidades humanas que habitan el Corredor Biológico.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Comisión Monteverde Brilla</strong><br>
                        </p>
                        <p>Comisión de personas voluntarias de la comunidad, con participación del Consejo Municipal, encargados del planeamiento y ejecución del Festival Monteverde Brilla todos los primeros jueves de diciembre.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Comisión de Enlace Carretera para Monteverde</strong><br>
                        </p>
                        <p>Comité que representa a la comunidad de Monteverde ante instituciones del estado, en lo que respecta a la construcción de la ruta 606.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Red ProNativas:</strong><br>
                        </p>
                        <p>Organización que vela por educar a las personas sobre la importancia, reproducción y uso de las plantas nativas ornamentales. <a href="http://www.pronativas.org" target="_blank">www.pronativas.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Corredor Biológico Pájaro Campana (CBPC):</strong><br>
                        </p>
                        <p>Iniciativa que proporciona conectividad entre paisajes, ecosistemas y hábitats naturales o modificados entre la zona montañosa de Monteverde y la zona de manglar en la costa pacífica. <a href="http://www.cbpc.org" target="_blank">www.cbpc.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Asociación Conservacionista de Monteverde (ACM):</strong><br>
                        </p>
                        <p>Organización civil, sin fines de lucro que busca conservar, preservar y rehabilitar ecosistemas tropicales y su biodiversidad. Desarrolla proyectos de importancia social, ambiental y económica. <a href="http://www.acmcr.org" target="_blank">www.acmcr.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Fondo Comunitario Monteverde (FCM):</strong><br>
                        </p>
                        <p>Organización filantrópica que une recursos y actores estratégicos de la comunidad para impulsar iniciativas de sostenibilidad en Monteverde y sus comunidades aledañas. <a href="http://www.monteverdefund.org" target="_blank">www.monteverdefund.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/01.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Cámara de Empresarios Turísticos de Monteverde:</strong><br>
                        </p>
                        <p>Ente local que unifica y organiza las empresas turísticas de la comunidad para el desarrollo de la misma.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h2>Iniciativas nacionales</h2>
        <div class="owl-sustaniability-01">
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Cámara Nacional de Ecoturismo y Turismo Sostenible (CANAECO)</strong><br>
                        </p>
                        <p>Organización no-gubernamental, sin fines de lucro, de carácter privado. Busca impulsar el desarrollo de la actividad turística del país de manera responsable y sostenible.  <a href="http://www.canaeco.org" target="_blank">www.canaeco.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Cámara Nacional de Turismo (CANATUR)</strong><br>
                        </p>
                        <p>Es una organización sin fines de lucro, que reúne a las cámaras y asociaciones de la empresa turística del país. <a href="http://www.canatur.org" target="_blank">www.canatur.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Red de Reservas Privadas</strong><br>
                        </p>
                        <p>Organización que agrupa las reservas privadas de Costa Rica, impulsando la conservación voluntaria. <a href="http://www.reservasprivadascr.org">www.reservasprivadascr.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Sello Pyme Costa Rica</strong><br>
                        </p>
                        <p>Sello que identifica a pequeñas y medianas empresas que cumple con sus obligaciones tributarias, cargas sociales de la CCSS, póliza de riesgos de trabajo. <a href="http://www.pyme.go.cr" target="_blank">www.pyme.go.cr</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Código de Conducta</strong><br>
                        </p>
                        <p>Es un compromiso que ha adquirido la industria turística costarricense para desestimular y sancionar la Explotación Sexual Comercial de Niñas, Niños y Adolescentes (ESCNNA), como un eje del turismo responsable y sostenible. <a href="http://www.paniamor.org" target="_blank">www.paniamor.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Programa de Bandera Azul Ecológica (PBAE)</strong><br>
                        </p>
                        <p>Galardón que se otorga anualmente, el cual premia el esfuerzo y el trabajo voluntario en la búsqueda de la conservación y el desarrollo, en concordancia con la protección de los recursos naturales, la implementación de acciones para enfrentar el cambio climático, la búsqueda de mejores condiciones higiénico sanitarias y la mejora de la salud pública.  <a href="http://www.banderaazulecologica.org" target="_blank">www.banderaazulecologica.org</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Programa de Bandera Verde o Sello de Calidad Sanitaria</strong><br>
                        </p>
                        <p>Es un galardón otorgado por el Instituto Costarricense de Acueductos y Alcantarillados (AyA). Pretende promover la participación de la sociedad civil para mejorar la operación y el mantenimiento de los acueductos en el país.</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="grid column-35-65">
                    <div>
                        <img src="<?= vpth_path('/img/sustainability/sliders/02.jpg') ?>" alt="">
                    </div>
                    <div>
                        <p class="by">
                            <strong>Certificación para la Sostenibilidad Turística (CST)</strong><br>
                        </p>
                        <p>Es un programa del Instituto Costarricense de Turismo (ICT), diseñado para diferenciar empresas turísticas de acuerdo al grado en el que su operación y funcionamiento se acerque a un modelo de sostenibilidad. <a href="http://www.turismo-sostenible.co.cr" target="_blank">www.turismo-sostenible.co.cr</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <h2>Normativa Ambiental Costarricense</h2>
        <div class="owl-sustaniability-01">

            <div class="item">
                <p class="by">
                    <strong>Ley sobre Patrimonio Nacional Arqueológico (Ley N° 6703)</strong><br>
                </p>
                <p>En Los Pinos-Cabañas &amp; Jardines reconocemos la importancia de nuestro patrimonio nacional arqueológico por lo que comprometidos defendemos este recurso tan valioso. Nos encontramos comprometidos con la protección de todos aquellos bienes culturales arqueológicos definidos por la Ley N° 6703, como todos aquellos productos de las actividades de los grupos humanos del pasado (específicamente, indígenas anteriores o contemporáneas al establecimiento de la cultura hispánica en territorio nacional). Con este fin procedemos a través de las siguientes acciones ante cualquier situación que atente en contra del patrimonio nacional arqueológico:</p>

                <p>&bull; Capacitación de nuestros colaboradores (as) sobre la Ley Nº 6703 de Patrimonio Nacional Arqueológico</p>
                <p>&bull; Divulgación de dicha Ley.</p>
                <p>&bull; Denuncia formal al Museo Nacional en caso de alguna situación que amenace dicho patrimonio.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Ley Orgánica del Ambiente (Ley N° 7554)</strong><br>
                </p>
                <p>En cumplimiento de lo establecido por la LOA, procuramos que nuestro desarrollo se lleve a cabo en armonía con la naturaleza procurando que cada una de nuestras actividades se lleve a cabo de manera responsable y respetuosa con el ambiente. Velamos por la contaminación mínima de: suelo, aire, ríos, áreas silvestres, bosques y cualquier otro considerado dentro de esta ley. Dentro de nuestra operación desarrollamos estos aspectos a través de programas de Gestión Integral de Residuos, Gestión Integral de los recursos Hídrico y Energético, así como a través de un Programa de Responsabilidad Social en el que incluimos la educación ambiental.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Ley de la Biodiversidad (Ley N° 7778)</strong><br>
                </p>
                <p>Nos encontramos comprometidos con la conservación de la biodiversidad y el uso sostenible de los recursos, por lo que hemos hechos estos ejes prioritarios en nuestra operación. Nuestra propiedad fue inscrita como Reserva Privada desde el año 2011. Hemos implementado en nuestra empresa un Programa de Monitoreo de especies así como medidas para la utilización sostenible de nuestros recursos. También somos parte del Corredor Biológico Pájaro Campana, mediante el cual trabajamos como comunidad y región en beneficio de la conectividad de los ecosistemas montañosos de Monteverde y los de manglar en la costa de Puntarenas.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Ley Contra la Explotación Sexual Comercial Infantil (Ley N° 7899)</strong><br>
                </p>
                <p>En Los Pinos-Cabañas &amp; Jardines trabajamos en defensa de la integridad y los derechos de los y las costarricenses, en general, adultos, adolescentes, niños y niñas. Por lo que desarrollamos nuestras actividades turísticas bajo los más estrictos parámetros de responsabilidad y vigilancia, siempre alertas para denunciar cualquier actividad de Explotación Sexual Comercial de Niños, Niñas y Adolescentes (ESCNNA). Nuestros colaboradores y colaboradoras han sido capacitados y capacitadas en este tema, nos hemos comprometido con la divulgación de esta información a todos nuestros públicos de interés.</p>
                <p>Trabajamos en una cultura de “cero tolerancia” contra todas las formas de explotación de niñas, niños y adolescentes, por lo que también participamos de la iniciativa país en el sector turismo del Código de Conducta y motivamos a otras empresas de nuestra localidad, región y país a formar parte del mismo.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Ley sobre Estupefacientes, Sustancias Psicotrópicas y Drogas de Uso No Autorizado (Ley N° 8204)</strong><br>
                </p>
                <p>En la propiedad de Los Pinos-Cabañas &amp; Jardines es terminantemente prohibido algún tipo de trato o comercio relacionado con drogas, estupefacientes y/o sustancias psicotrópicas de uso no autorizado. En Costa Rica este tipo de acciones es penalizado con cárcel. Si se diera algún hecho similar se tomarán las medidas respectivas en términos de denuncia (en caso de clientes o proveedores involucrados) o despido (en caso de funcionarios involucrados), esto según nuestro protocolo de denuncia correspondiente.</p>
            </div>

            <div class="item">
                <p class="by">
                    <strong>Ley Contra el Hostigamiento Sexual en el Empleo y la Docencia (Ley N° 7476)</strong><br>
                </p>
                <p>En Los Pinos-Cabañas &amp; Jardines no se permitirá que ninguno de las y los integrantes del personal incurra en una situación de hostigamiento sexual-laboral, de ser así este o esta será despedido de manera inmediata según lo establecido por esta ley, sin responsabilidad patronal y/o sanciones en términos de pagos por indemnización a la víctima e incluso prisión. La empresa sancionará severamente cualquier indicio de acoso sexual hacia cualquier persona o cualquier situación en la que se vea afectada (o) alguno de sus colaboradores durante gestión laboral dentro o fuera de las instalaciones de la empresa.</p>
            </div>
        </div>
    </div>

    <div class="container">
        <h2>Reglamento para Visitas a Áreas Protegidas</h2>
        <div class="owl-sustaniability-01">

            <div class="item">
                <p class="by">
                    En Costa Rica, desde 1991 se han venido publicando mediante decreto ejecutivo los reglamentos de uso público para las áreas silvestres protegidas que reciben visitantes en mayor número. Para cada sitio, se definen las zonas destinadas al uso público y las actividades y los usos permitidos, se regulan los horarios de visita y la observación de especies, se definen las actividades no permitidas y las prohibiciones. Es de suma importancia que todos los visitantes de nuestras áreas protegidas estén en conocimiento de estos reglamentos, por lo que les recomendamos informarse directamente con el sitio web del Sistema de Parques Nacionales (SINAC)
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Es fundamental que los turistas cooperen con una adecuada disposición de los desechos sólidos que generen, separándolos y depositándolos en recipientes y lugares asignados para ello.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Toda visita en áreas protegidas debe realizarse con una mentalidad conservacionista y actitud de protección del medio ambiente y defensa y/o rescate de la vida silvestre.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    La propiedad de los Pinos Cabañas &amp; Jardines fue inscrita como reserva privada desde el año 2011. Procurando en nuestra propiedad aire más puro hemos declarado esta como un espacio libre de tabaco, por lo que solicitamos de manera muy respetuosa no fumar dentro de nuestra propiedad. Esta restricción se establece como reglamento, medida que rige, tanto para el staff, clientes y visitantes en general.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    En caso de llevar alimentos a las áreas naturales, asegúrese de no alimentar a los animales y de no depositar en estos sitios restos de los mismos con el fin de evitar cualquier impacto que genere en la fauna local.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Procure evitar, al máximo, todo tipo de perturbación innecesaria a la vida silvestre, así como al hábitat mismo. Debe minimizarse el ruido al caminar dentro de los senderos, manteniendo su distancia de las especies y no tocar las plantas.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Respete la demarcación y camine siempre dentro de los senderos señalizados, evitando el salirse de ellos.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Si al ingresar a un sendero se encuentra otro grupo de turistas, debe mantenerse una distancia prudencial entre ambos grupos para no alterar el medio de manera excesiva.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Las zonas turísticas en la que se ubica Los Pinos-Cabañas &amp; Jardines ofrece grandes atractivos turísticos, comerciales y culturales, por lo que le sugerimos visitar las comunidades aledañas. De esta manera, nos ayudara a apoyar a los pobladores, mediante la adquisición de bienes y servicios en la localidad, motivándolos para una interacción positiva de la comunidad y colaborando al soporte de la actividad conservacionista.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    No adquiera souvenirs y/o recuerdos de su viaje, o artículos artesanales, elaborados con especies silvestres, maderas en peligro de extinción, ni tampoco las especies mismas. De esta manera evitara ser sancionado por las autoridades locales, pues nuestra Ley de Conservación de Vida Silvestre # 7495 prohíbe y penaliza con fuertes multas la adquisición, la comercialización y/o tenencia de artículos de este tipo.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    En Costa Rica nos hemos declarado defensores de los niños, niñas y adolescentes que pudieran ser amenazados por la explotación sexual comercial infantil. Si observa situación que sugiera algún indicio de esta despreciable actividad, háganoslo saber para proceder de inmediato según el procedimiento del Código de Conducta Internacional, entidad a la cual nos hemos aliado muchas empresas de turismo para luchar unidos contra este tipo de acciones tan censurables. En Costa Rica se castiga con pena de cárcel sin posibilidad de fianza hasta un mínimo de 8 años.
                </p>
            </div>

            <div class="item">
                <p class="by">
                    Al igual que con el punto anterior, es importante tomar en cuenta nuestras leyes # 8204 contra el consumo de sustancias ilegales (tenencia, consumo y comercio) y la # 7476 contra el hostigamiento sexual. En Los Pinos-Cabañas &amp; Jardines, hemos capacitado a nuestro personal sobre estos temas, brindando orientación adecuada y estableciendo un reglamento interno que garantice un ambiente de respeto y de armonía para todos.
                </p>
            </div>
        </div>
    </div>
</section>

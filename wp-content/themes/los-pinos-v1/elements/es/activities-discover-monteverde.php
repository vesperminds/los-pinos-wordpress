<section class="discovery">
    <div class="container">
        <h2>Descubra las actividades que Monteverde le ofrece.</h2>
    </div>

    <div class="discovery-list discovery-monteverde grid column-4">
        <a href="#" data-id="0" class="active">
            <img src="<?= vpth_path('/img/discovery/01_b.jpg') ?>" alt="">
            <p>Reserva Biológica<br>de Monteverde</p>
        </a>
        <a href="#" data-id="1">
            <img src="<?= vpth_path('/img/discovery/02_b.jpg') ?>" alt="">
            <p>Reserva de<br>Santa Elena</p>
        </a>
        <a href="#" data-id="2">
            <img src="<?= vpth_path('/img/discovery/03_b.jpg') ?>" alt="">
            <p>Puentes<br>Colgantes</p>
        </a>
        <a href="#" data-id="3">
            <img src="<?= vpth_path('/img/discovery/04_b.jpg') ?>" alt="">
            <p>Tour de<br>Tirolinas</p>
        </a>
    </div>

    <div class="container">
        <div class="discovery-item active">
            <p>Esta reserva biológica privada es administrada por el CCT (Centro Científico Tropical) organización sin fines de lucro más que por la conservación del bosque. Contiene 6 distintas zonas de vida. Su bosque es llamado nuboso y no bosque lluvioso debido a su altitud que permite atrapar mejor las nubes de la atmósfera. Las copas de sus árboles están llenas de aves, mariposas, insectos y cientos de plantas. Se recomienda traer un abrigo impermeable o un pocho y zapatos apropiados para caminatas. Botas de hule no son comúnmente necesarias por debido al buen estado de los senderos. Abierto diariamente de 07:00 a 16:00. Entrada: Adulto US $18, niño y estudiante con identificación US $9 Las caminatas de historia natural $15 + entrada. Ubicada a 4 kilómetros de las cabañas.</p>
        </div>
        <div class="discovery-item">
            <p>Estas 765 acres de reserva están localizas a 6 Km. al noreste del pueblo de Santa Elena. Pertenece al Colegio Técnico Profesional de Santa Elena y todos los fondos recaudados por ella son destinados a la educación de la zona y a la preservación del bosque. Es menos visitada que su vecina de Monteverde. Existen 10 km. de senderos que le muestran bellezas como un mirador al Volcán Arenal que en días claros se logra ver imponentemente. En ella se puede optar por caminatas guiadas o caminar por sus senderos por su propia cuenta con solo comprar una auto-guía. Abre diariamente de 07:00 hasta las 16:00. La entrada regular es de $15 y de $7 para estudiantes con ID.</p>
        </div>
        <div class="discovery-item">
            <p>Es una manera de ver el bosque desde las alturas de unos puentes que le brindan seguridad y disfrute al recorrer los distintos niveles, los puentes forman un recorrido por el bosque nuboso entre montañas y depresiones. Abierto todos los días de 07:00 a 16:00. El rango de precios va desde los $16 a niños hasta los a los $30 para adultos. Las caminatas guiadas estás disponibles con reservación previa. Duración aproximada 1:30 horas. El transporte es gratis en algunas compañías.</p>
        </div>
        <div class="discovery-item">
            <p>Esto es aventura pura cargada de adrenalina, le toma a usted desde una plataforma en un árbol hasta el siguiente árbol a través de cables muy seguros al mejor estilo de “Tarzán” que le permite descubrir los secretos de la aventura y la naturaleza al mismo tiempo. Durante el tour usted podrá observar plantas epifitas, monos, aves y muchas otras cosas bellas de estos bosques. ¡De seguro recordará esta aventura durante toda su vida! Hay algunos horarios del transporte gratis. Las entradas van desde los $30 para niños hasta los $45 para adultos. Se requiere reservación. Duración aproximada 3 horas.</p>
        </div>
    </div>
    <div class="container">
         <a href="<?= vpth_path('/books/monteverde.pdf') ?>" class="btn bordergreen" target="_blank">DESCARGAR ACTIVIDADES DE MONTEVERDE</a>
    </div>
</section>

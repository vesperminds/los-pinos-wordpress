<section id="main-map">
    <div class="container">
        <h2>Mapa</h2>
        <p>
            ¿Estás planeando visitarnos?<br>
            Por favor revisa el mapa de la Reserva Los Pinos.
        </p>
        <br><br>
        <img class="fullwidth" src="<?= vpth_path('/img/about-reserve/map.jpg') ?>" alt="">
    </div>
</section>

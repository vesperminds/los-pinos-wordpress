<section id="about-reserve">
    <div class="container">
        <h2>Reserva Privada Los Pinos</h2>
        <p>Somos parte del corredor del pájaro campana que conecta el Bosque Nuboso con los manglares en la costa pacífica.</p>
        <p>Para atraer más flora y fauna a nuestra propiedad, en los últimos dos años hemos reemplazando las plantas exóticas y plantas ornamentales cultivadas en invernaderos, por plantas nativas que requieren menos cuidados y menos riego durante el verano. Ahora podemos observar más vida salvaje y podemos detectar periódicamente aves y mamíferos a lo largo de todo el año, todo lo que necesita es unos binoculares y ya está listo para una corta caminata por nuestros senderos.</p>
        <p>40% de nuestros 7.2 hectáreas (18 acres) se dedican a la operación comercial con la renta de cabañas y producción de vegetales y el otro 60% se dedica a la conservación y reforestación. Somos parte del corredor del pájaro campana que conecta el Bosque Nuboso con los manglares en la costa pacífica.</p>

        <ul class="reserve-icons grid column-4">
            <li>
                <img src="<?= vpth_path('/img/about-reserve/01.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/02.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/03.jpg') ?>" alt="">
            </li>
            <li>
                <img src="<?= vpth_path('/img/about-reserve/04.jpg') ?>" alt="">
            </li>
        </ul>
    </div>
</section>

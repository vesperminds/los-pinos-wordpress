<section class="discovery">
    <div class="container">
        <h2>¿Planea visitarnos?</h2>
        <p>Por favor revise la siguiente lista en caso de que necesite información adicional, puede llamarnos al (506) 2645-5252 o contactarnos por correo electrónico info@lospinos.net</p>
    </div>

    <div class="container">
        <ul class="fromlist discovery-locations">
            <li><a data-from="9.9333333,-84.0833333" href="#" target="_blank" class="btn sanjose greenborder" data-id="0">Desde San José Centro</a></li>
            <li><a data-from="9.9972531,-84.2124366" href="#" target="_blank" class="btn airport" data-id="1">Desde el Aeropuerto Intl. Juan Santamaría</a></li>
            <li><a data-from="10.6341788,-85.4420377" href="#" target="_blank" class="btn liberia" data-id="2">Desde Liberia</a></li>
            <li><a data-from="10.4708657,-84.6458391" href="#" target="_blank" class="btn lafortuna" data-id="3">Desde La Fortuna</a></li>
            <li><a data-from="10.2998322,-85.8412277" href="#" target="_blank" class="btn tamarindo" data-id="4">Desde Tamarindo</a></li>
            <li><a data-from="9.9773799,-84.8327200" href="#" target="_blank" class="btn puntarenas" data-id="5">Desde Puntarenas</a></li>
            <li><a data-from="10.312275,-84.814918" data-to="9.9773799,-84.8327200" href="#" target="_blank" class="btn montetopunta" data-id="6">Desde Monteverde hacia Puntarenas</a></li>
        </ul>
        <div class="contenedor-discovery">
            <div class="discovery-item active">
                <div class="come-alone">
                    <h2>DESDE SAN JOSÉ CENTRO</h2>
                    <p>
                        Buses salen a las 6:30 AM y a las 2:30 PM. Pregunte por el bus llamado: Terminal 7-10. Duración: 4.5 hrs. Costo: ¢2.810 por persona cada vía.
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>DESDE EL AEROPUERTO INTL. JUAN SANTAMARÍA</h2>
                    <p>
                        Tome un bus o un taxi a “Multicentro La Estación” en la entrada a Alajuuela, el bus pasa por aquí y no frente al aeropuerto. Nosotros recomendamos que de ser possible, usted vaya a la estación del bus en San José y compre su ticket, para asegurar su asiento.
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>DESDE LIBERIA</h2>
                    <p>
                        Tome un bus a San José o Puntarenas y baje en Sardinal, ahí espere uno de los buses rumbo a Monteverde. Hay buses que pasan a las 9:00 AM y a las 5:00 PM.
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>DESDE LA FORTUNA</h2>
                    <p>
                        Es un poco complicado debido a que hay solo un buspor día viniendo de Tilarán, así que nosotros recomendamos tomar el servicio de jeep boat jeep por $25 - $35. (es un transporte desde su hotel hasta el lago, hay un bote lo pasa hasta el otro extreme, y de ahí otro transporte lo trae hasta Los Pinos).
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>DESDE TAMARINDO</h2>
                    <p>
                        Tome un bus a San José o Puntarenas y baje en Sardinal, ahí espere uno de los buses rumbo a Monteverde. Hay buses que pasan a las 9:00 AM y a las 5:00 PM.
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM PUNTARENAS</h2>
                    <p>
                        Hay 3 buses disponibles: uno sale a las 8:00 AM, otro a la 1:30 PM y el último a las 2:15 PM. El bus de la 1:30 PM pasa por La Irma alrededor de las 2:30 PM y llega a Monteverde a las 5:00 PM. El bus de las 2:15 PM pasa por Lagartos alrededor de las 3:00 PM y llega a Monteverde cerca de las 6:00 PM. ¢1.500 por persona cada vía.
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>DESDE MONTEVERDE HACIA PUNTARENAS</h2>
                    <p>
                        Hay 3 buses disponibles: uno sale a las 8:00 AM, otro a la 1:30 PM y el último a las 2:15 PM. El bus de la 1:30 PM pasa por La Irma alrededor de las 2:30 PM y llega a Monteverde a las 5:00 PM. El bus de las 2:15 PM pasa por Lagartos alrededor de las 3:00 PM y llega a Monteverde cerca de las 6:00 PM. ¢1.500 por persona cada vía.
                    </p>
                    <a href="#" class="own-direction">Si está por su cuenta, revise la dirección aquí</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--
https://www.google.com/maps/dir/San+José,+Costa+Rica/Los+Pinos+-+Cabañas+%26+Jardines,+150+E+escuela+pública,+Cerro+Plano,+Monteverde,+77-5655/@10.3340185,-85.0019923,10z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8fa0741c479806c7:0x50a56999f5fc77fa!2m2!1d-84.52562!2d10.49797!1m5!1m1!1s0x8fa01a2b426f85c3:0x72f116a5452bbb0b!2m2!1d-84.814918!2d10.311979
https://www.google.com/maps/dir/Liberia,+Guanacaste,+Costa+Rica/Los+Pinos+-+Cabañas+%26+Jardines,+150+E+escuela+pública,+Cerro+Plano,+Monteverde,+77-5655/@10.4389148,-85.2708378,11z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8f757d2a8a1c08e5:0xb2f8a494fb368393!2m2!1d-85.4406747!2d10.6345964!1m5!1m1!1s0x8fa01a2b426f85c3:0x72f116a5452bbb0b!2m2!1d-84.814918!2d10.311979
https://www.google.com/maps/dir/Puntarenas,+Costa+Rica/Los+Pinos+-+Cabañas+%26+Jardines,+150+E+escuela+pública,+Cerro+Plano,+Monteverde,+77-5655/@10.1463605,-84.9249847,11z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8fa02eb96dada6b7:0x3f48bd110f83d5ec!2m2!1d-84.8294211!2d9.9778439!1m5!1m1!1s0x8fa01a2b426f85c3:0x72f116a5452bbb0b!2m2!1d-84.814918!2d10.311979
https://www.google.com/maps/dir/Monteverde,+Puntarenas,+Costa+Rica/Puntarenas,+Costa+Rica/@10.1263554,-84.9259881,11z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8fa0190abebeddc9:0x6655a93f1b3ad827!2m2!1d-84.8255097!2d10.2749682!1m5!1m1!1s0x8fa02eb96dada6b7:0x3f48bd110f83d5ec!2m2!1d-84.8294211!2d9.9778439
https://www.google.com/maps/dir/Aeropuerto+Internacional+Juan+Santamaría,+Autopista+Bernardo+Soto,+Río+Segundo,+Alajuela,+Costa+Rica/Los+Pinos+-+Cabañas+%26+Jardines,+150+E+escuela+pública,+Cerro+Plano,+Monteverde,+77-5655/@10.101143,-84.6677036,11z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8fa0f9a32e823a41:0xc545f9616eaf5b01!2m2!1d-84.2102479!2d9.9972531!1m5!1m1!1s0x8fa01a2b426f85c3:0x72f116a5452bbb0b!2m2!1d-84.814918!2d10.311979
https://www.google.com/maps/dir/Tamarindo,+Guanacaste,+Costa+Rica/Los+Pinos+-+Caba%C3%B1as+%26+Jardines,+150+E+escuela+p%C3%BAblica,+Cerro+Plano,+Monteverde,+77-5655/@10.3690133,-85.6083873,10z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8f9e39409203c30f:0xbb189f5e2cc1f893!2m2!1d-85.8371058!2d10.2992746!1m5!1m1!1s0x8fa01a2b426f85c3:0x72f116a5452bbb0b!2m2!1d-84.814918!2d10.311979
https://www.google.com/maps/dir/La+Fortuna,+Alajuela,+Costa+Rica/Los+Pinos+-+Caba%C3%B1as+%26+Jardines,+150+E+escuela+p%C3%BAblica,+Cerro+Plano,+Monteverde,+77-5655/@10.2781525,-84.8659757,11z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x8fa00c890660e999:0x69e3a87bd2572ce6!2m2!1d-84.6426806!2d10.4678335!1m5!1m1!1s0x8fa01a2b426f85c3:0x72f116a5452bbb0b!2m2!1d-84.814918!2d10.311979
-->

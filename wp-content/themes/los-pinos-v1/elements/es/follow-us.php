<div id="follow-us">
    <p>
        <strong>Los Pinos - Cabañas y Jardines</strong><br>
        Monteverde, Puntarenas, Costa Rica <br>
        Teléfono: (506)2645-5252 <br>
        Correo electrónico: info@lospinos.net
    </p>
    <ul>
        <li>
            <a href="https://www.facebook.com/lospinoscr/" target="_blank" class="social fb">
                <img src=<?= vpth_path('/img/icons/facebook.svg') ?> alt="">
            </a>
        </li>
        <li>
            <a href="https://twitter.com/lospinoscr" target="_blank" class="social twitter">
                <img src=<?= vpth_path('/img/icons/twitter.svg') ?> alt="">
            </a>
        </li>
        <li>
            <a href="https://www.tripadvisor.com/Hotel_Review-g309277-d644744" target="_blank" class="social tripadvisor">
                <img src=<?= vpth_path('/img/icons/tripadvisor.svg') ?> alt="">
            </a>
        </li>
    </ul>
</div>

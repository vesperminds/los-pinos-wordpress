<section id="header">
    <div class="content">
        <h2>Es difícil capturar nuestra esencia</h2>
        <p>en fotografías, pero lo hemos intentado</p>
    </div>
</section>

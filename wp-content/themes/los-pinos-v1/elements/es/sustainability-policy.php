<section class="discovery">
    <div class="container">
        <h2>Política de Sostenibilidad</h2>

        <p>Los Pinos-Cabañas &amp; Jardines se compromete a brindar servicios en el campo de la hotelería y alojamiento, asegurando a sus clientes la calidad, seguridad y efectividad en los mismos, dando cumplimiento a lo determinado por las autoridades legales y otros organismos competentes.</p>
        <p>Estos servicios serán ejecutados mediante la igual involucración de los ámbitos social, ambiental y económico, procurando un uso racional de los recursos, así como una participación activa de la empresa en las comunidades cercanas.</p>
        <p>Este compromiso es extendido a nuestro personal y todas las actividades que el mismo ejecute, apuntando siempre al liderazgo, innovación y mejora continua en nuestro cometido.</p>
    </div>

    <div class="discovery-list discovery-aditional-policy grid column-3">
        <a href="#" data-id="0">
            <img src="<?= vpth_path('/img/sustainability/01.jpg') ?>" alt="">
            <p>Misión<br>&nbsp;</p>
        </a>
        <a href="#" data-id="1">
            <img src="<?= vpth_path('/img/sustainability/02.jpg') ?>" alt="">
            <p>Visión<br>&nbsp;</p>
        </a>
        <a href="#" data-id="2">
            <img src="<?= vpth_path('/img/sustainability/03.jpg') ?>" alt="">
            <p>Nuestro<br>Círculo Verde</p>
        </a>
    </div>

    <div class="container">
        <div class="discovery-item active">
            <p>Ofrecer un alojamiento de calidad en comunión con la naturaleza, que le permita al visitante vivir una experiencia en Monteverde, mediante un servicio familiar autóctono, de empatía y comunidad.</p>
        </div>
        <div class="discovery-item">
            <p>Llegar a ser las mejores cabañas de Monteverde, que con acciones sostenibles en nuestra gestión cotidiana, logren satisfacer las necesidades de nuestro huésped y el entorno inmediato, hoy mañana y siempre.</p>
        </div>
        <div class="discovery-item">
            <p>Como parte de nuestro Plan de Sostenibilidad hemos implementado el Programa de Responsabilidad Social Empresarial a través de este hemos logrado involucrarnos en los siguientes proyectos, estos nos han permitido desenvolvernos como mejores empresarios aportando a nuestra localidad, región y país un granito de arena.</p>
        </div>
    </div>

    <div class="container">
        <a href="<?= vpth_path('/downloads/turismo_sostenible.pdf') ?>" class="btn greenborder nonwidth">NUESTRAS ACCIONES SOSTENIBLES</a>
        <br><br>

        <p>Viajar de manera responsable y limpia ahora es posible. El Fondo Nacional de Financiamiento Forestal de Costa Rica ha habilitado una calculadora que permite calcular las emisiones de su vuelo y compensar las mismas.</p>
        <a href="http://www.fonafifo.go.cr/home/investments/carbon_calculator.html" target="_blank" class="btn greenborder nonwidth">CALCULE SU HUELLA DE CARBONO</a>
    </div>
</section>

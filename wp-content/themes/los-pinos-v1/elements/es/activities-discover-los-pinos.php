<section class="discovery">
    <div class="container">
        <h2>Descubra las actividades que Los Pinos le ofrece.</h2>
    </div>

    <div class="discovery-list discovery-lospinos grid column-4">
        <a href="#" data-id="0" class="active">
            <img src="<?= vpth_path('/img/discovery/01_a.jpg') ?>" alt="">
            <p>Jardín<br>hidropónico</p>
        </a>
        <a href="#" data-id="1">
            <img src="<?= vpth_path('/img/discovery/02_a.jpg') ?>" alt="">
            <p>Senderos<br>&nbsp;</p>
        </a>
        <a href="#" data-id="2">
            <img src="<?= vpth_path('/img/discovery/03_a.jpg') ?>" alt="">
            <p>Área de<br>niños</p>
        </a>
        <a href="#" data-id="3">
            <img src="<?= vpth_path('/img/discovery/04_a.jpg') ?>" alt="">
            <p>Mirador<br>&nbsp;</p>
        </a>
    </div>

    <div class="container">
        <div class="discovery-item active">
            <p>La hidroponía o cultivo sin suelo ha conseguido estándares comerciales, y que algunos alimentos, plantas ornamentales y jóvenes plantas de tabaco se cultivan de esta manera por diversas razones que tienen que ver con la falta de suelos adecuados; por suelos contaminados por microorganismos que producen enfermedades a las plantas o por usar aguas subterráneas que degradaron la calidad de esos suelos.</p>

            
        </div>
        <div class="discovery-item">
            <p>Mientras se camina por los senderos, los visitantes podrán apreciar hermosos panoramas expuestos de forma natural por la división continental, considerado como un accidente geográfico que recorre la cordillera principal de Costa Rica.</p>

            
        </div>
        <div class="discovery-item">
            <p>El juego es una actividad vital para el niño. Jugar es imprescindible para el buen desarrollo físico, emocional, social e intelectual. Con el juego el niño se expresa, se comunica y descubre todo lo que le rodea mientras interactúa con su entorno.</p>

            
        </div>
        <div class="discovery-item">
            <p>Hermosos panoramas expuestos de forma natural por la división continental, considerado como un accidente geográfico que recorre la cordillera principal de Costa Rica.</p>

        </div>
    </div>
</section>

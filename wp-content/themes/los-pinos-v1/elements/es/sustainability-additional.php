<section class="discovery">

        <div class="discovery-list discovery-aditional grid column-2">
            <a href="#" data-id="0">
                <img src="<?= vpth_path('/img/sustainability/05.jpg') ?>" alt="">
                <p>Sitios web<br>recomendados</p>
            </a>
            <a href="#" data-id="1">
                <img src="<?= vpth_path('/img/sustainability/06.jpg') ?>" alt="">
                <p>¡Queremos escucharle!</p>
            </a>
        </div>
    <div class="container">

        <div class="discovery-item active">
            <ul>
                <li>
                    <a href="http://www.visitcostarica.com/" target="_blank">Costa Rican Institute of Tourism (ICT)</a>
                </li>
                <li><a href="http://www.turismo-sostenible.co.cr/" taret="_blank">Certification for Sustainable Tourism (CST)</a></li>
                <li>
                    <a href="http://www.turismo-sostenible.co.cr/" target="_blank">Ministry of the Environment</a>
                </li>
                <li>
                    <a href="http://www.ministrabajo.go.cr/" target="_blank">Ministry of Labour and Social Security</a>
                </li>
                <li>
                    <a href="http://www.ccad.ws/" target="_blank">Central American Commission on Environment and Development</a>
                </li>
                <li>
                    <a href="http://www.inbio.ac.cr/" target="_blank">National Institute of Biodiversity (INBIO)</a>
                </li>
                <li>
                    <a href="http://www.preserveplan.org/" target="_blank">APREFLOFAS</a>
                </li>
                <li>
                    <a href="http://www.anaicr.org/anai/en/presentation.html" target="_blank">ANAI Asociation</a>
                </li>
                <li>
                    <a href="http://www.cccturtle.org/" target="_blank">Corp. The Caribbean Conservation</a>
                </li>
                <li>
                    <a href="http://www.catie.co.cr/" target="_blank">CATIE</a>
                </li>
                <li>
                    <a href="http://www.cct.or.cr/" target="_blank">Tropical Science Center</a>
                </li>
                <li>
                    <a href="http://www.cinpe.una.ac.cr/" target="_blank">CINPE</a>
                </li>
                <li>
                    <a href="http://www.earth.ac.cr/" target="_blank">Universidad EARTH</a>
                </li>
                <li>
                    <a href="http://www.fonafifo.com/" target="_blank">FONAFIFO (forestry financing fund)</a>
                </li>
                <li>
                    <a href="http://www.neotropica.org/" target="_blank">Neotropic Foundation</a>
                </li>
                <li>
                    <a href="http://www.fod.ac.cr/" target="_blank">Fundación Omar Dengo</a>
                </li>
                <li>
                    <a href="http://www.paniamor.or.cr/" target="_blank">Fundación Paniamor</a>
                </li>
                <li>
                    <a href="http://www.fundecor.org/" target="_blank">FUNDECOR</a>
                </li>
                <li>
                    <a href="http://www.ots.ac.cr/es/" target="_blank">Organization tropical studies</a>
                </li>
                <li>
                    <a href="http://www.rainforest-alliance.org/" target="_blank">Rainforest Alliance</a>
                </li>
                <li>
                    <a href="http://www.sinac.go.cr/" target="_blank">SINAC</a>
                </li>
            </ul>
        </div>
        <div class="discovery-item">
            <p>Evalúe nuestro desempeño y ayúdenos a mejorar:</p>
            <ul>
                <li>
                    <a href="https://docs.google.com/forms/d/1RrISIc2ySsraDM5igR3DyN_-e8YasDWaucKzUfDRr5U/viewform" target="_blank">Clientes</a>
                </li>
                <li>
                    <a href="https://docs.google.com/forms/d/1Miaz3l5-CGQUw5IWOjniT0z1kM-Q6AWrsBk1RypEPJk/viewform" target="_blank">Vecinos de la zona de Monteverde y alrededores</a>
                </li>
            </ul>
            <p>
                O háganos llegar sus opiniones, comentarios y(o) sugerencias a: sostenibilidad@lospinos.net
            </p>
        </div>
    </div>
</section>

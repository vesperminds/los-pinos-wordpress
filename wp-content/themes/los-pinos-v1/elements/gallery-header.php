<section id="header">
    <div class="content">
        <h2>It's hard to capture our essence</h2>
        <p>in pictures, but we've tried!</p>
    </div>
</section>

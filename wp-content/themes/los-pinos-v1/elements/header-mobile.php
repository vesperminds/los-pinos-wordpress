<?php
    $langList = [];
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages();

        if (!empty($languages)) {
            foreach ($languages as $language) {
                if (!empty($language['active'])) {
                    continue;
                }

                $langList[] = [
                    'abbr' => $language['language_code'],
                    'name' => $language['native_name'],
                    'url' => $language['url']
                ];
            }
        }
    }
?>
<div id="mobile-header">
    <a class="openmenu"></a>
    <img src=<?= vpth_path('/img/logo.png') ?> alt="" class="mobile-menu">
</div>

<nav id="main-mobile-menu">
    <a href="<?= vp_url(_x('/', 'path for / page', 'p')) ?>"><?= __('Home', 'p') ?></a>
    <a href="<?= vp_url(_x('/about-us', 'path for /about-us page', 'p')) ?>"><?= __('Los Pinos', 'p') ?></a>
    <a href="<?= vp_url(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ?>"><?= __('Rooms &amp; Rates', 'p') ?></a>
    <a href="<?= vp_url(_x('/activities', 'path for /activities page', 'p')) ?>"><?= __('Activities', 'p') ?></a>
    <a href="<?= vp_url(_x('/location', 'path for /location page', 'p')) ?>"><?= __('Location', 'p') ?></a>
    <a href="<?= vp_url(_x('/gallery', 'path for /gallery page', 'p')) ?>"><?= __('Gallery', 'p') ?></a>
    <a href="<?= vp_url(_x('/sustainability', 'path for /sustainability page', 'p')) ?>"><?= __('Sustainability', 'p') ?></a>
    <a href="<?= vp_url(_x('/library', 'path for /library page', 'p')) ?>"><?= __('Library', 'p') ?></a>
    <a href="<?= vp_url(_x('/reserve', 'path for /reserve page', 'p')) ?>"><?= __('Reserve', 'p') ?></a>
    <a href="<?= vp_url(_x('/contact-us', 'path for /contact-us page', 'p')) ?>"><?= __('Contact us', 'p') ?></a>
    <?php if ($langList): ?>
    <a href="<?= $langList[0]['url'] ?>" class="lang"><?= $langList[0]['name'] ?></a>
    <?php endif; ?>
</nav>

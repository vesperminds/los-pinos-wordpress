<?php
    $weather = tt_get_weather_info();

    $langList = [];
    if (function_exists('icl_get_languages')) {
        $languages = icl_get_languages();

        if (!empty($languages)) {
            foreach ($languages as $language) {
                if (!empty($language['active'])) {
                    continue;
                }

                $langList[] = [
                    'abbr' => $language['language_code'],
                    'name' => $language['native_name'],
                    'url' => $language['url']
                ];
            }
        }
    }
?>
<header>
    <div class="subheader">
        <div class="container">
            <?php if ($weather): ?>
            <div id="weather" class="<?= $weather['icon'] ?>" alt="<?= $weather['icon'] ?>"><em><?= round($weather['temperature'], 0) ?>&deg;C</em></div>
            <?php endif; ?>
            <ul>
                <li><a class="<?= vp_is_active(_x('/sustainability', 'path for /sustainability page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/sustainability', 'path for /sustainability page', 'p')) ?>"><?= __('SUSTAINABILITY', 'p') ?></a></li>
                <li><a class="<?= vp_is_active(_x('/library', 'path for /library page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/library', 'path for /library page', 'p')) ?>"><?= __('LIBRARY', 'p') ?></a></li>
                <li><a class="<?= vp_is_active(_x('/reserve', 'path for /reserve page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/reserve', 'path for /reserve page', 'p')) ?>"><?= __('RESERVE', 'p') ?></a></li>
                <li><a class="<?= vp_is_active(_x('/contact-us', 'path for /contact-us page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/contact-us', 'path for /contact-us page', 'p')) ?>"><?= __('CONTACT US', 'p') ?></a></li>
                <?php if ($langList): ?>
                <li><a href="<?= $langList[0]['url'] ?>"><?= $langList[0]['abbr'] ?></a></li>
                <?php else :?>
                <li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
    <div class="container">
        <nav>
            <ul>
                <li>
                    <a class="<?= vp_is_active('@home-page') ? 'active--link' : '' ?>" href="<?= vp_url(_x('/', 'path for / page', 'p')) ?>"><?= __('Home', 'p') ?></a>
                </li>
                <li>
                    <a class="<?= vp_is_active(_x('/about-us', 'path for /about-us page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/about-us', 'path for /about-us page', 'p')) ?>"><?= __('Los Pinos', 'p') ?></a>
                </li>
                <li>
                    <a class="<?= vp_is_active(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ?>"><?= __('Rooms &amp; Rates', 'p') ?></a>
                </li>
                <li class="logo">
                    <a class="<?= vp_is_active('@home-page') ? 'active--link' : '' ?>" href="<?= vp_url(_x('/', 'path for / page', 'p')) ?>"><img src=<?= vpth_path('/img/logo.png') ?> alt=""></a>
                </li>
                <li>
                    <a class="<?= vp_is_active(_x('/activities', 'path for /activities page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/activities', 'path for /activities page', 'p')) ?>"><?= __('Activities', 'p') ?></a>
                </li>
                <li>
                    <a class="<?= vp_is_active(_x('/location', 'path for /location page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/location', 'path for /location page', 'p')) ?>"><?= __('Location', 'p') ?></a>
                </li>
                <li>
                    <a class="<?= vp_is_active(_x('/gallery', 'path for /gallery page', 'p')) ? 'active--link' : '' ?>" href="<?= vp_url(_x('/gallery', 'path for /gallery page', 'p')) ?>"><?= __('Gallery', 'p') ?></a>
                </li>
            </ul>
        </nav>
    </div>
</header>
<!-- NOTE: class="current-page" -->

<section id="header">
    <div class="content">
        <h2>A natural accommodation built and managed</h2>
        <p>by a family native to Monteverde.</p>
    </div>
</section>

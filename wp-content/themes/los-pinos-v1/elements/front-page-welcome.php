<section id="welcome">
    <div class="container">
        <h2>Welcome to Los Pinos - Cabins &amp; Gardens</h2>
        <p>Established in 1987, our family-friendly and highly sustainable lodging facilities are now a reference in the area.</p>
        <p>Enjoy our fully equipped cabins, immersed in mind-soothing nature, surrounded by beautiful native gardens and exuberant forest, built far apart from each other for extreme privacy and equipped with all cooking essentials. Explore our 8 hectare private reserve and/or spot wildlife right outside your window! All while being close to all the awesome activities that Monteverde has to offer.</p>
        <p>Take our private onsite trails up to our wonderful viewpoint and to our Hydroponic Vegetable Garden, where you will be able to harvest a variety of vegetables that you can consume during your stay at our cabins.</p>
        <p>All of this makes us your best sustainable alternative in this world renowned ecotourism destiny, which is Monteverde. Come with your family or friends and stay away from the stress that cities and congested areas can cause. If you don’t want to cook, don’t worry, there are restaurants and other amenities nearby, just 400 meters from Los Pinos!</p>
        <div class="qualifications grid column-4">
            <div>
                <img src=<?= vpth_path('/img/qualification/01.jpg') ?> alt="">
            </div>
            <div>
                <img src=<?= vpth_path('/img/qualification/02.jpg') ?> alt="">
            </div>
            <div>
                <img src=<?= vpth_path('/img/qualification/03.jpg') ?> alt="">
            </div>
            <div>
                <img src=<?= vpth_path('/img/qualification/04.jpg') ?> alt="">
            </div>
        </div>
    </div>
</section>

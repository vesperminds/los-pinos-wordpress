<section class="discovery">
    <div class="container">
        <h2>Are you planning to visit us?</h2>
        <p>Please check out the following list but In case you need any additional information, you may call us at (506) 2645-5252 or contact us by email to info@lospinos.net</p>
    </div>

    <div class="container">
        <ul class="fromlist discovery-locations">
            <li><a data-from="9.9333333,-84.0833333" href="#" target="_blank" class="btn sanjose greenborder" data-id="0">From San José Center</a></li>
            <li><a data-from="9.9972531,-84.2124366" href="#" target="_blank" class="btn airport" data-id="1">From Juan Santamaría Intl. Airport</a></li>
            <li><a data-from="10.6341788,-85.4420377" href="#" target="_blank" class="btn liberia" data-id="2">From Liberia</a></li>
            <li><a data-from="10.4708657,-84.6458391" href="#" target="_blank" class="btn lafortuna" data-id="3">From La Fortuna</a></li>
            <li><a data-from="10.2998322,-85.8412277" href="#" target="_blank" class="btn tamarindo" data-id="4">From Tamarindo</a></li>
            <li><a data-from="9.9773799,-84.8327200" href="#" target="_blank" class="btn puntarenas" data-id="5">From Puntarenas</a></li>
            <li><a data-from="10.312275,-84.814918" data-to="9.9773799,-84.8327200" href="#" target="_blank" class="btn montetopunta" data-id="6">From Monteverde to Puntarenas</a></li>
        </ul>
        <div class="contenedor-discovery">
            <div class="discovery-item active">
                <div class="come-alone">
                    <h2>FROM SAN JOSÉ CENTER</h2>
                    <p>
                        Buses depart at 6:30 AM or at 2:30 PM. Ask for the bus stop called: Terminal 7-10. Duration: 4.5 hrs. Cost: ¢2.810 per person each way.
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM JUAN SANTAMARÍA INTL. AIRPORT</h2>
                    <p>
                        Take a bus or taxi to “Multicentro la Estación” on the entrance to Alajuela, where the bus that departs from San José goes through. It DOES NOT go through the airport on the way to Monteverde. We recommend going all the way to the bus station in San José, if possible, to buy a ticket and make sure you have a seat.
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM LIBERIA</h2>
                    <p>
                        Take a Bus to San José or Puntarenas and get off in Sardinal, and then wait for one of the Monteverde buses to go by. The last bus goes by around 5 pm. Some buses go in through Sardinal and others go in further up north.
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM LA FORTUNA</h2>
                    <p>
                        It is quite complicated as there is only one bus a day coming from Tilarán to Monteverde, so we recommend taking the $20-25 jeep boat jeep. (A shuttle from your hotel to the lake, a boat across and another shuttle from the lake directly to Los Pinos).
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM TAMARINDO</h2>
                    <p>
                        Take a Bus to San José or Puntarenas and get off in Sardinal, and then wait for one of the Monteverde buses to go by. The last bus goes by around 5 pm. Some buses go in through Sardinal and others go in further up north.
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM PUNTARENAS</h2>
                    <p>
                        There are 3 buses: one departing at 8:00am, another at 1:30 PM and one at 2:15 PM. The 1:30 PM bus goes through La Irma at about 2:30 PM and arrives in Monteverde at 5:00 PM. The 2:15 PM bus goes through Lagartos at around 3:00 PM and gets to Monteverde at around 6:00 PM. ¢1.500 per person each way.
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
            <div class="discovery-item">
                <div class="come-alone">
                    <h2>FROM MONTEVERDE TO PUNTARENAS</h2>
                    <p>
                        There are three buses: one at 4:20 am, one at 6:00 am and one at 3:00pm. The two last ones take the Lagartos exit. The first one goes through Las Juntas and exits through La Irma.
                    </p>
                    <a href="#" class="own-direction">If you are by your own, check directions here</a>
                </div>
            </div>
        </div>
    </div>
</section>

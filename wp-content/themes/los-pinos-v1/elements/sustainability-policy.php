<section class="discovery">
    <div class="container">
        <h2>Sustainability Policy</h2>

        <p>Los Pinos-Cabañas &amp; Jardines is committed to provide services in the field of accommodation, ensuring customers the quality, safety and effectiveness in these, giving achievement to what is specified in the law and by other competent bodies.</p>
        <p>These services will be implemented through the equal involvement of social, environmental and economic fields, to ensure a rational use of resources, as well as an active participation of the company in nearby communities.</p>
        <p>This commitment is extended to our staff and all the activities they achieve, always pointing to leadership, innovation and continuous improvement of our tasks.</p>
    </div>

    <div class="discovery-list discovery-aditional-policy grid column-3">
        <a href="#" data-id="0">
            <img src="<?= vpth_path('/img/sustainability/01.jpg') ?>" alt="">
            <p>Mission<br>&nbsp;</p>
        </a>
        <a href="#" data-id="1">
            <img src="<?= vpth_path('/img/sustainability/02.jpg') ?>" alt="">
            <p>Vision<br>&nbsp;</p>
        </a>
        <a href="#" data-id="2">
            <img src="<?= vpth_path('/img/sustainability/03.jpg') ?>" alt="">
            <p>Our Green<br>Circle</p>
        </a>
    </div>

    <div class="container">
        <div class="discovery-item active">
            <p>Offer a quality accommodation service in communion with nature, allowing the visitors to experience Monteverde through a family-like, empathy and community setting.</p>
        </div>
        <div class="discovery-item">
            <p>Become the best cabins of Monteverde, which through sustainable actions in our day-to-day activity; fulfill the needs of our guests and the immediate surroundings, today tomorrow and always.</p>
        </div>
        <div class="discovery-item">
            <p>As part of our Sustainability Plan we have implemented a Corporate Social Responsibility Program, the same has permitted us to get involved in the following projects, these have enabled us to manage our activities as best entrepreneurs contributing to our town, region and country.</p>
        </div>
    </div>

    <div class="container">
        <a href="<?= vpth_path('/downloads/sustainable_tourism.pdf') ?>" class="btn greenborder nonwidth">OUR SUSTAINABLE ACTION</a>
        <br><br>

        <p>Clean and responsible traveling is now possible. The National Fund of Forest Financing of Costa Rica (FONAFIFO) has enabled a calculator that allows you to calculate your flight emissions and compensate them.</p>
        <a href="http://www.fonafifo.go.cr/home/investments/carbon_calculator.html" target="_blank" class="btn greenborder nonwidth">CALCULATE YOUR CARBON FOOTPRINT</a>
    </div>
</section>

<section id="about-monteverde">
    <div class="container">
        <h2>About Monteverde</h2>

        <div class="borderline grid column-2">
            <div>
                <div><strong>Average temperature: </strong> <p>16-22 C (61-72 F)</p></div>
                <div><strong>Annual rainfall: </strong> <p>3,000 mm (118 inches)</p></div>
                <div><strong>Dry Season: </strong> <p>December to March.</p></div>
                <div><strong>Located: </strong>  <p>225 km northwest. of San Jose, 3h00 by<br>4x4, 4h00 in a sedan, 5h by bus.</p></div>
            </div>
            <div>
                <div>
                    <div><strong>Population of Monteverde: </strong> <p> 250</p></div>
                    <div><strong>Population of Santa Elena: </strong> <p> 6,500</p></div>
                    <div><strong>Altitude, Santa Elena village: </strong> <p>1,250 meters or 4,100 feet,</p></div>
                    <div><strong>Highest trails: </strong>  <p>1,800 meters or 5,900 feet.</p></div>
                </div>
            </div>
        </div>

        <p>The region is found right on the Continental Divide at around 1500 meters (4920 ft) above sea level, where the Santa Elena and Monteverde Cloud Forest Reserves are found, being this one of the most interesting places to visit in Costa Rica.</p>
        <p>The area is acclaimed for being one of the most outstanding wildlife refuges in the New World Tropics. Monteverde is known as an international ecotourism destination and one of the top places to visit in Costa Rica.</p>
        <p>Within the Monteverde Cloud Forest, more than 100 species of mammals, including 5 species of cats, are found. In addition the following flora and fauna can be found:</p>
        <p>
            - 400 species of birds, including 30 kinds of hummingbirds <br>
            - 500 species of butterflies <br>
            - 120 reptile and amphibian species<br>
            - 2,500 plant species (among them 420 different types of orchids and 200 fern species)<br>
            - 500 species of trees<br>
            - Thousands of insect species.
        </p>
        <p>
            Among the 400 species of birds, some of the best known species are: The Quetzal, the Bell Bird, the Black Guan, the Mot-Mot, the Orange-bellied Trogon, and the Bare-neck Umbrella Bird.
        </p>
        <p>Some of the mammals found are the Collared Wild Pig, the Small Deer, the Margay, the Spider and Howler Monkey, the Coatimundi, the Kinkajou, the Olingo, the Agouti and the Paca, the Two-toed Sloth, the Armadillo and the Weasel.</p>
        <br><br>
        <h3>
            Now it’s time to look for a comfortable shelter.<br>
            Check out our
        </h3>
        <br>
        <a href="<?= vp_url(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ?>" class="btn black">Rooms &amp; Rates</a>
    </div>
</section>

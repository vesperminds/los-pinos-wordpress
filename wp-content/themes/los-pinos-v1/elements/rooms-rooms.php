<?php
$rooms = get_all_rooms([
    'content' => true,
    'meta' => true,
    'limit' => 6
]);
?>
<section id="rooms">
    <div class="container">
        <h2><?= __('Our Rooms', 'p') ?></h2>
        <p><?= __('All our cabins offer a space where you can spread out and feel completely at ease. We’ll make sure you’ll never want anything else when staying with us.', 'p') ?></p>

        <ul class="book-filters">
            <li class="active">
                <a href="" data-book="all"><?= __('All', 'p') ?> <em></em></a>
            </li>
            <?php foreach ($rooms as $room): ?>
            <li>
                <a href="#" data-book="room-<?= $room['id'] ?>"><?= $room['title'] ?> <em></em></a>
            </li>
            <?php endforeach; ?>
        </ul>

    </div>

    <ul class="room-list grid column-<?= count($rooms) ?>">
        <?php foreach ($rooms as $room): ?>
        <?php
            $main_lb_img = false;
            $extra_lb_img = [];

            if (!empty($room['images'])) {
                $main_lb_img = array_shift($room['images']);
                $extra_lb_img = $room['images'];
            }
        ?>
        <li class="room-<?= $room['id'] ?>">
            <?php if ($main_lb_img): ?>
            <a href="<?= $main_lb_img[0] ?>" data-lightbox="room-<?= $room['id'] ?>">
                <img src="<?= $room['thumb'] ?>" alt="">
            </a>
            <?php else: ?>
            <img src="<?= $room['thumb'] ?>" alt="">
            <?php endif; ?>
            <?= $room['content'] ?>
            <?php foreach ($extra_lb_img as $image): ?>
            <a href="<?= $image[0] ?>" data-lightbox="room-<?= $room['id'] ?>" style="display: none;"></a>
            <?php endforeach; ?>
        </li>
        <?php endforeach; ?>

    </ul>

    <ul class="book-now-list grid column-<?= count($rooms) ?>">
        <?php foreach ($rooms as $room): ?>
        <li>
            <a href="https://hotels.cloudbeds.com/reservation/Vy3xrw" target="_blank" class="btn greenborder nonwidth"><?= __('Book Now', 'p') ?></a>
        </li>
        <?php endforeach; ?>
    </ul>

</section>

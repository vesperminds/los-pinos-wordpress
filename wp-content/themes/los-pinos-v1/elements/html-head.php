<!DOCTYPE html>
<html lang="en" class="Site">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?= tt_title() ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="msapplication-tap-highlight" content="no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<link rel="stylesheet" href="<?= vpth_path('/dist/css/style.css') ?>?v=<?= vpth_version('/dist/css/style.css') ?>" media="screen" charset="utf-8">
<link rel="stylesheet" href="<?= vpth_path('/js/pickadate/themes/default.date.css') ?>" media="screen" charset="utf-8">
<link rel="stylesheet" href="<?= vpth_path('/js/pickadate/themes/default.css') ?>" media="screen" charset="utf-8">
<link rel="stylesheet" href="<?= vpth_path('/lib/lightbox2/css/lightbox.min.css') ?>" media="screen" charset="utf-8">
<link rel="icon" href="<?= vpth_path('/favicon.ico') ?>" type="image/x-icon"/>
<link rel="shortcut icon" href="<?= vpth_path('/favicon.ico') ?>" type="image/x-icon"/>
</head>
<body <?= body_class() ?> ng-controller="siteCtrl">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79828016-1', 'auto');
  ga('send', 'pageview');

</script>

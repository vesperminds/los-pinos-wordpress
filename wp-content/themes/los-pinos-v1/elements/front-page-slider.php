<section id="slider">
    <div class="owl-carousel-home homeslider">
        <div class="item" style="background: url('<?= vpth_path('/img/headers/slider-home.jpg') ?>')"></div>
        <div class="item"></div>
    </div>
    <div class="check-in">
        <p><?= __('Amazing ecotourism destination', 'p') ?></p>
        <h2>
            <?= __('between the Santa Elena and<br>Monteverde Cloud Forest Reserves', 'p') ?>
        </h2>
        <a href="<?= vp_url(_x('/rooms-rates', 'path for /rooms-rates page', 'p')) ?>" class="btn green"><?= __('CHECK OUT OUR ROOMS &amp; RATES', 'p') ?></a>
        <div class="checkin-panels grid column-2">
            <div>
                <form id="check-availability" action="" class="grid">

                    <fieldset>
                        <p><?= __('Check In', 'p') ?></p>
                        <img src=<?= vpth_path('/img/icons/date.svg') ?> alt="" class="icon date"><input type="text" name="checkIn" id="checkIn" placeholder="<?= __('Select a Date', 'p') ?>"/>
                    </fieldset>

                    <fieldset>
                        <p><?= __('Check Out', 'p') ?></p>
                        <img src=<?= vpth_path('/img/icons/date.svg') ?> alt="" class="icon date"><input type="text" name="checkOut" id="checkOut" placeholder="<?= __('Select a Date', 'p') ?>"/>
                    </fieldset>

                </form>
            </div>
            <div>
                <a href="#" class="btn check" lp-action="availability" lp-form="#check-availability"><?= __('CHECK AVAILABILITY', 'p') ?></a>
            </div>
        </div>
    </div>
</section>

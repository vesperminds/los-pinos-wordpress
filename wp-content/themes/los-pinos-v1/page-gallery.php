<?php if (!empty($_GET['_ajax'])): ?>

<?php get_translated_template_part('elements/gallery', 'photos') ?>

<?php else: ?>

<?php tt_body_class('galery-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/gallery', 'header') ?>
<?php get_translated_template_part('elements/gallery', 'photos') ?>
<?php get_translated_template_part('elements/gallery', 'shelter') ?>
<?php get_translated_template_part('elements/gallery', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

<?php endif; ?>

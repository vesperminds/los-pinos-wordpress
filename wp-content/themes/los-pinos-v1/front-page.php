<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/front-page', 'slider') ?>
<?php get_translated_template_part('elements/front-page', 'welcome') ?>
<?php get_translated_template_part('elements/front-page', 'room-rates') ?>
<?php get_translated_template_part('elements/front-page', 'tripadvisor-slider') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

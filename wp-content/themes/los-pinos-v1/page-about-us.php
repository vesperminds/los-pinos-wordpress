<?php tt_body_class('about-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/about-us', 'header') ?>
<?php get_translated_template_part('elements/about-us', 'welcome') ?>
<?php get_translated_template_part('elements/about-us', 'team') ?>
<?php get_translated_template_part('elements/about-us', 'monteverde') ?>
<?php get_translated_template_part('elements/about-us', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

��    /      �  C           
        $  �   (     �     �     �        
         +  	   4  2   >  
   q     |  0  �     �      �     �     �     �     �     �       	     	      	   *     4     =     E     M     _     n  a   |     �     �  ?        H  !   Z  %   |  %   �     �     �  !   	     *	  '   J	  -   r	     �	  �  �	     x     �  �   �  #   5     Y     b  #   u     �     �     �  2   �     �     �  e  �     a  ,   j     �     �  
   �  
   �     �  
   �  	   �     �     �     
          !     )     @     O  N   U     �     �  =   �                    -     =     I     U     c     o     �     �     &       '              )       "   #   .               ,              	                       $          %                                  (                       -                  *                             +   
   /   !           Activities All All our cabins offer a space where you can spread out and feel completely at ease. We’ll make sure you’ll never want anything else when staying with us. Amazing ecotourism destination Book Now CHECK AVAILABILITY CHECK OUT OUR ROOMS &amp; RATES CONTACT US Check In Check Out Check out some photos and videos from our reserve. Contact us Download Each and every one of our collaborators represent our spirit. Our success is based on our unique culture and work atmosphere. We are passionate and proud, we care for the environment, the well-being of our collaborators and community, and are focused on exceeding our guest’s expectations at all times. English Every image has a story to tell! Gallery Home LIBRARY Library Load more photos Location Los Pinos Our Books Our Rooms Our Team RESERVE Reserve Rooms &amp; Rates SUSTAINABILITY Select a Date Simply click on the thumbnail to preview a slide gallery of the images and navigate through them. Sustainability all photo typesAll photos between the Santa Elena and<br>Monteverde Cloud Forest Reserves path for / page/ path for /about-us page/about-us path for /activities page/activities path for /contact-us page/contact-us path for /gallery page/gallery path for /library page/library path for /location page/location path for /reserve page/reserve path for /rooms-rates page/rooms-rates path for /sustainability page/sustainability photo type filterView: Project-Id-Version: Pinos
POT-Creation-Date: 2016-11-10 01:02-0300
PO-Revision-Date: 2016-11-10 01:02-0300
Last-Translator: Emi <edgardo.balbuena@majesticmedia.ca>
Language-Team: VesperMinds
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js/greensock
 Actividades Todas Todas nuestras cabañas le ofrecen un espacio donde puede sentirse completamente a gusto. Vamos a asegurarnos de que nunca faltará nada durante su estancia con nosotros. Sorprendente destino ecoturístico, Reservar VER DISPONIBILIDAD VER NUESTRAS HABITACIONES Y PRECIOS CONTÁCTENOS Ingreso Salida Check out some photos and videos from our reserve. Contáctenos Bajar Todos y cada uno de nuestros colaboradores representan nuestro espíritu. Nuestro éxito se basa en nuestra atmósfera y la cultura de trabajo único. Somos apasionados y orgullosos, cuidamos del medio ambiente, el bienestar de nuestros colaboradores y de la comunidad, y estamos enfocados en exceder las expectativas de nuestros huéspedes en todo momento. Español ¡Cada imagen tiene una historia que contar! Galería Inicio BIBLIOTECA Biblioteca Cargar más fotos Ubicación Los Pinos Nuestros libros Nuestras habitaciones Nuestro equipo RESERVA Reserva Habitaciones y Precios SOSTENIBILIDAD Fecha Simplemente haga clic en la vista previa y navegue a través de las imágenes. Sostenibilidad Todos las fotos entre la Reserva de Santa Elena<br>y la Reserva de Monteverde /es/ /es/acerca-de-nosotros /es/actividades /es/contactenos /es/galeria /biblioteca /es/ubicacion /es/reserva /es/habitaciones-y-precios /es/sostenibilidad Ver: 
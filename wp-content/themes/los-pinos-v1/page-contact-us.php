<?php
tt_handle_form();
?>
<?php tt_body_class('about-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/contact-us', 'header') ?>
<?php get_translated_template_part('elements/contact-us', 'form') ?>
<?php get_translated_template_part('elements/contact-us', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

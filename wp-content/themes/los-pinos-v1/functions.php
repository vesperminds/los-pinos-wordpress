<?php defined('ABSPATH') or die;

define('DEBUG_MODE', strpos($_SERVER['SERVER_NAME'], '0.0.0.0') !== false);

/* Scripts and stylesheet */

add_action('wp_enqueue_scripts', function() {

    $scripts = [
        [ 'jquery', '/js/jquery-2.1.4.min.js' ],
        [ 'owl_carousel', '/js/owl.carousel.min.js' ],
        [ 'pickadate', '/js/pickadate/picker.js' ],
        [ 'pickadate-date', '/js/pickadate/picker.date.js' ],
        [ 'pickadate-polyfill', '/js/pickadate/legacy.js' ],
        [ 'date-new', '/js/pickadate/picker.date.js' ],
        [ 'fancybox', '/js/jquery.fancybox.js' ],
        [ 'lightbox2', '/lib/lightbox2/js/lightbox.min.js' ],
        [ 'main', '/js/app.js' ],
        [ 'booking', '/js/booking.js' ],
        [ 'contact', '/js/contact.js' ],
    ];

    $lang = defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en' ?
        substr(ICL_LANGUAGE_CODE, 0, 2) : '';

    if ($lang == 'es') {
        $scripts[] = [ 'pickadate-translation', '/js/pickadate/translations/es_ES.js' ];
    }

    foreach ($scripts as $script) {

        $ext = isset($script[2]) && $script[2];

        wp_deregister_script($script[0]);
        wp_register_script(
            $script[0],
            $ext ? $script[1] : vpth_path($script[1]),
            null,
            $ext ? md5($script[1]) : vpth_version($script[1]),
            true
        );

    }

    foreach($scripts as $script) {
        wp_enqueue_script($script[0]);
    }

});

/* Title */

$_overrideTitle = null;

function tt_title_suffix() {
    return ' &mdash; Los Pinos - Monteverde, Costa Rica';
}

function tt_set_page_title($title = 'Los Pinos - Monteverde, Costa Rica') {

    global $_overrideTitle;

    if (empty($title)) {
        return;
    }

    $_overrideTitle = $title;

}

function tt_title() {
    return wp_title(tt_title_suffix(), true, 'right');
}

add_filter('wp_title', function($title) {
    global $_overrideTitle;

    if (!empty($_overrideTitle)) {
        return $_overrideTitle . tt_title_suffix();
    }

    if (empty($title) && (is_home() || is_front_page())) {
        return 'Los Pinos - Monteverde, Costa Rica';
    }

    return $title;
});

/* Body class */

$_appendBodyClass = '';

function tt_body_class($class = '') {
    global $_appendBodyClass;

    $_appendBodyClass = $class;
}

add_filter('body_class', function($classes) {
    global $_appendBodyClass;

    if ($_appendBodyClass) {
        $classes = array_merge($classes, explode(' ', $_appendBodyClass));
    }

    return $classes;
});

/* Images */

add_theme_support('post-thumbnails', [
	'post',
	'page',
	'team_member',
	'room',
	'photo',
	'book',
]);

add_action('init', function() {

	add_image_size('vesper-hero-image', 1920, 720);
	add_image_size('vesper-mobile-hero-image', 960, 360);
	add_image_size('vesper-post-thumbnail', 600, 600, true);
	add_image_size('vesper-post-thumbnail-cover', 600, 600);
	add_image_size('vesper-gallery-image', 1200, 1200);
	add_image_size('vesper-book-cover', 480, 640);

});

/* JSON Attrs */

function tt_json_attr($data = null) {
    return htmlspecialchars(json_encode($data), ENT_QUOTES);
}

/* Weather */

function tt_get_weather_info() {
    return V_Weather::build('monteverde')->currently();
}

/* Helper shortcodes */

add_shortcode('section', function($attr, $content = '') {
	return '<p>' . do_shortcode(nl2br(trim($content))) . '</p>';
});


/* JSON */

function tt_json_output($data) {

	@header('Content-Type: application/json; charset=utf-8');
	echo json_encode($data);

}

/* Form (Contact and Tour Info) */

define('EMAIL_EOL', "\r\n");

$tt_form_handlers = [

	'contact' => function($step, $data) {

		if ($step == 'validate') {

			$errors = [];

			if (empty($data['fullname'])) {
				$errors[] = 'fullname';
			}

			if (empty($data['phone'])) {
				$errors[] = 'phone';
			}

			if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
				$errors[] = 'email';
			}

			if (empty($data['comments'])) {
				$errors[] = 'comments';
			}

			return $errors;

		}
		elseif ($step == 'compose') {

			$message = [];

			$message[] = 'Contact Request';
			$message[] = '---------------';
			$message[] = '';
			$message[] = 'Full Name: ' . strip_tags($data['fullname']);
			$message[] = 'Phone: ' . strip_tags($data['phone']);
			$message[] = 'Email: ' . strip_tags($data['email']);
			$message[] = 'Language: ' . strip_tags($data['lang']);
			$message[] = 'Message:';
			$message[] = strip_tags($data['comments']);

			return $message;

		}
		elseif ($step == 'subject') {
			return 'Contact Form';
		}
		elseif ($step == 'compose-user') {

			$message = [];

			$message[] = 'Hey ' . $data['fullname'] . ',';
			$message[] = '';
			$message[] = 'Thank you for getting in touch with us. We\'ll reply you as soon as possible.';
			$message[] = '';
			$message[] = 'Thanks,';
			$message[] = '- Los Pinos team';


			return $message;

		}
		elseif ($step == 'subject-user') {
			return 'Thank you for contacting us';
		}

		return false;

	},
];

function tt_handle_form() {

	global $tt_form_handlers;

	if ($_SERVER['REQUEST_METHOD'] != 'POST') {
		return;
	}

	if (empty($_POST['_fh']) || empty($tt_form_handlers[$_POST['_fh']])) {
		return;
	}

	$fh = $_POST['_fh'];

	$data = $_POST;

	foreach ($data as $key => $value) {
		$data[$key] = trim(strip_tags($value));
	}

	$validationErrors = $tt_form_handlers[$fh]('validate', $data);

	if (!empty($validationErrors)) {
		tt_json_output([
			'done' => false,
			'error' => 'E_INVALID_FIELDS',
			'message' => 'One or more fields are not valid',
			'details' => $validationErrors
		]);
		exit;
	}

	$admin_email = get_option('admin_email');
	$admin_rcpt = [ $admin_email, 'vesperminds@gmail.com' ];

	if (!DEBUG_MODE) {
		$admin_rcpt[] = 'info@lospinos.net';
    }

    $admin_rcpt = array_unique($admin_rcpt);

	$subject = $tt_form_handlers[$fh]('subject', $data);
	$message = $tt_form_handlers[$fh]('compose', $data);

	if (!DEBUG_MODE) {
		wp_mail($admin_rcpt, $subject, implode(EMAIL_EOL, $message));
	}

	$subjectUser = $tt_form_handlers[$fh]('subject-user', $data);
	$messageUser = $tt_form_handlers[$fh]('compose-user', $data);

	if (!DEBUG_MODE) {
		wp_mail($data['email'], $subjectUser, implode(EMAIL_EOL, $messageUser));
	}

	tt_json_output([
		'done' => true
	]);
	exit;

}

function get_translated_template_part($slug, $name = null) {
    $templates = [];
	$name = (string) $name;
    $lang = defined('ICL_LANGUAGE_CODE') && ICL_LANGUAGE_CODE != 'en' ?
        substr(ICL_LANGUAGE_CODE, 0, 2) : '';

    if ($lang) {
        $translated_slug = str_replace('\\', '/', $slug);
        $translated_slug_slashpos = strrpos($translated_slug, '/');

        $translated_slug = substr_replace(
            $translated_slug,
            $lang . '/',
            $translated_slug_slashpos !== false ? $translated_slug_slashpos + 1: 0,
            0
        );

        if ( '' !== $name )
    		$templates[] = "{$translated_slug}-{$name}.php";

        $templates[] = "{$translated_slug}.php";
    }

	if ( '' !== $name )
		$templates[] = "{$slug}-{$name}.php";

	$templates[] = "{$slug}.php";

	locate_template($templates, true, false);
}

<?php tt_body_class('map-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/location', 'header') ?>
<?php get_translated_template_part('elements/location', 'discover') ?>
<?php get_translated_template_part('elements/location', 'shelter') ?>
<?php get_translated_template_part('elements/location', 'follow-us') ?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<?php get_translated_template_part('elements/html', 'tail') ?>

<?php tt_body_class('sustainability-page') ?>

<?php get_translated_template_part('elements/html', 'head') ?>
<?php get_translated_template_part('elements/header', 'mobile') ?>
<?php get_translated_template_part('elements/header') ?>

<?php get_translated_template_part('elements/sustainability', 'header') ?>
<?php get_translated_template_part('elements/sustainability', 'policy') ?>
<?php get_translated_template_part('elements/sustainability', 'sliders') ?>
<?php get_translated_template_part('elements/sustainability', 'additional') ?>
<?php get_translated_template_part('elements/sustainability', 'shelter') ?>
<?php get_translated_template_part('elements/sustainability', 'follow-us') ?>

<?php get_translated_template_part('elements/html', 'tail') ?>

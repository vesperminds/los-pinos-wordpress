<?php defined('ABSPATH') or die;
/**
 * Plugin Name: VesperPress
 * Description: Wordpress framework for plugins and themes
 * Version: 1.7
 * Author: Vesper Minds
 * Author URI: http://vesperminds.com/
 * Network: true
 */
define('DISALLOW_FILE_EDIT', true);
define('VESPERPRESS', 1);

include __DIR__ . '/VesperPress/theme.php';
include __DIR__ . '/VesperPress/post.php';
include __DIR__ . '/VesperPress/metabox.php';

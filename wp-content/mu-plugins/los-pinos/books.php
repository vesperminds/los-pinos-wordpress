<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Books */

    register_post_type('book', [
        'labels' => [
            'name' => 'Books',
            'singular_name' => 'Book',
            'menu_name' => 'Books',
            'name_admin_bar' => 'Book',
            'add_new' => 'Add new',
            'add_new_item' => 'Add New Book',
            'new_item' => 'New Book',
            'edit_item' => 'Edit Book',
            'view_item' => 'View Book',
            'all_items' => 'All Books',
            'search_items' => 'Search Books',
            'parent_item_color' => 'Parent Books:',
            'not_found' => 'No books found.',
            'not_found_in_trash' => 'No books found in Trash',
        ],
        'public' => true,
        'menu_position' => 35,
        'menu_icon' => 'dashicons-book',
        'capability_type' => 'page',
        'supports' => [
            'title',
            'editor',
            'thumbnail',
        ]
    ]);

    add_filter('user_can_richedit', function($value) {

        global $post;

        if ($post->post_type === 'book') return false;

        return $value;

    });

    /* Meta */

    VP_MetaBox::registerForCustomPostType('book', [
        'id' => 'book-download',
        'title' => 'Book Download',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'location' => [
                'label' => 'Book File',
                'type' => 'text',
            ],

        ]
    ]);

});

/* Books helper */

function get_all_books(array $opts = []) {

    $opts = array_merge([
		'type' => 'book',
		'thumbsize' => 'vesper-book-cover',
        'meta' => true,
	], $opts);

	return vp_get_all($opts);

}

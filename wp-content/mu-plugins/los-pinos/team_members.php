<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Tours */

    register_post_type('team_member', [
        'labels' => [
            'name' => 'Team Members',
            'singular_name' => 'Team Members',
            'menu_name' => 'Team Members',
            'name_admin_bar' => 'Team Member',
            'add_new' => 'Add new',
            'add_new_item' => 'Add New Team Member',
            'new_item' => 'New Team Member',
            'edit_item' => 'Edit Team Member',
            'view_item' => 'View Team Member',
            'all_items' => 'All Team Members',
            'search_items' => 'Search Team Members',
            'parent_item_color' => 'Parent Team Members:',
            'not_found' => 'No team members found.',
            'not_found_in_trash' => 'No team members found in Trash',
        ],
        'public' => true,
        'menu_position' => 35,
        'menu_icon' => 'dashicons-businessman',
        'capability_type' => 'page',
        'supports' => [
            'title',
            'editor',
            'thumbnail',
        ]
    ]);

    /* Meta */

    VP_MetaBox::registerForCustomPostType('team_member', [
        'id' => 'team-member-information',
        'title' => 'Team Member Information',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'role' => [
                'label' => 'Role',
                'type' => 'text',
                'maxlength' => 40,
            ]

        ]
    ]);

    add_filter('user_can_richedit', function($value) {

        global $post;

        if ($post->post_type === 'team_member') return false;

        return $value;

    });

});

/* Tours helper */

function get_all_team_members(array $opts = []) {

    $opts = array_merge([
		'type' => 'team_member',
		'thumbsize' => 'vesper-post-thumbnail'
	], $opts);

	return vp_get_all($opts);

}

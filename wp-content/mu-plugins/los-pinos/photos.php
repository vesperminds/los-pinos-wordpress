<?php defined('ABSPATH') or die;

add_action('init', function() {

	/* Tours */

	register_post_type('photo', [
		'labels' => [
			'name' => 'Photos',
			'singular_name' => 'Photo',
			'menu_name' => 'Gallery',
			'name_admin_bar' => 'Photo',
			'add_new' => 'Add new',
			'add_new_item' => 'Add New Photo',
			'new_item' => 'New Photo',
			'edit_item' => 'Edit Photo',
			'view_item' => 'View Photo',
			'all_items' => 'All Photos',
			'search_items' => 'Search Photos',
			'parent_item_color' => 'Parent Photos:',
			'not_found' => 'No photos found.',
			'not_found_in_trash' => 'No photos found in Trash',
		],
		'public' => true,
		'menu_position' => 30,
		'menu_icon' => 'dashicons-camera',
		'capability_type' => 'page',
		'supports' => [
			'title',
			'thumbnail'
		],
		'rewrite' => [
			'slug' => 'photo'
		]
	]);


	add_action('add_meta_boxes_photo', function() {

		remove_meta_box('postimagediv', 'photo', 'side');
	    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'photo', 'normal', 'high');

	});

	/* Photo types */

	register_taxonomy('photo_types', 'photo', [
		'labels' => [
			'name' => 'Photo Types',
			'singular_name' => 'Photo Type',
			'all_items' => 'All Photo Types',
			'edit_item' => 'Edit Photo Type',
			'view_item' => 'View Photo Type',
			'update_item' => 'Update Photo Type',
			'add_new_item' => 'Add New Photo Type',
			'new_item_name' => 'New Photo Type Name',
			'parent_item' => 'Parent Photo Type',
			'parent_item_colon' => 'Parent Photo Type:',
			'search_items' => 'Search Photo Types',
			'popular_items' => 'Popular Photo Types',
			'separate_items_with_commas' => 'Separate photo types with commas',
			'add_or_remove_items' => 'Add or remove photo types',
			'choose_from_most_used' => 'Choose from the most used photo types',
			'not_found' => 'No photo types found.',
			'menu_name' => 'Photo Types',
		],
		'public' => true,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => true,
		'hierarchical' => true
	]);
	register_taxonomy_for_object_type('photo_types', 'photo');

});

function get_all_photos(array $opts = []) {

    $opts = array_merge([
		'type' => 'photo',
		'thumbsize' => 'vesper-post-thumbnail-cover',
		'extraimage' => 'vesper-gallery-image'
	], $opts);

	return vp_get_all($opts);

}

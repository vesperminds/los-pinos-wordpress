<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Tours */

    register_post_type('room', [
        'labels' => [
            'name' => 'Rooms',
            'singular_name' => 'Rooms',
            'menu_name' => 'Rooms',
            'name_admin_bar' => 'Room',
            'add_new' => 'Add new',
            'add_new_item' => 'Add New Room',
            'new_item' => 'New Room',
            'edit_item' => 'Edit Room',
            'view_item' => 'View Room',
            'all_items' => 'All Rooms',
            'search_items' => 'Search Rooms',
            'parent_item_color' => 'Parent Rooms:',
            'not_found' => 'No rooms found.',
            'not_found_in_trash' => 'No rooms found in Trash',
        ],
        'public' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-admin-multisite',
        'capability_type' => 'page',
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'page-attributes'
        ]
    ]);

    /* Meta */

    VP_MetaBox::registerForCustomPostType('room', [
        'id' => 'room-information',
        'title' => 'Room Information',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'booking-id' => [
                'label' => 'Booking ID',
                'type' => 'text',
                'maxlength' => 60,
            ]

        ]
    ]);

});

/* Tours helper */

function get_all_rooms(array $opts = []) {

    $opts = array_merge([
		'type' => 'room',
        'order' => 'ASC',
        'orderby' => 'menu_order',
		'thumbsize' => 'vesper-post-thumbnail-cover',
	], $opts);

	$all = vp_get_all($opts);

    if (function_exists('get_images_src')) {
        array_walk($all, function(&$room) {
            $room['images'] = get_images_src('vesper-gallery-image', false, $room['id']);
        });
    }

    return $all;
}

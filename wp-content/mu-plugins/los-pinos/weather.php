<?php defined('ABSPATH') or die;

class V_Weather
{
    protected $coordinates = [null, null];
    protected $api_key;
    protected $data;

    public function setCoordinates($lat, $lng)
    {
        $this->coordinates = [$lat, $lng];
        return $this;
    }

    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function setApiKey($api_key)
    {
        $this->api_key = $api_key;
        return $this;
    }

    public function get($key = null)
    {
        if (!$this->data || $this->isDataExpired($this->data)) {
            $this->hydrate();
        }

        if ($key) {
            return isset($this->data['data'][$key]) ? $this->data['data'][$key] : null;
        }

        return $this->data['data'];
    }

    public function currently()
    {
        return $this->get('currently');
    }

    public function hydrate()
    {
        if (!$this->coordinates) {
            throw new \Exception('Missing coordinates');
        }

        $data = get_option($this->cacheKey());

        if ($data && !$this->isDataExpired($data)) {
            $this->data = $data;
            return $this;
        }

        $data = [
            'expires' => strtotime('+30 minutes'),
            'data' => $this->fetch()
        ];

        $this->data = $data;
        update_option($this->cacheKey(), $this->data, true);

        return $this;
    }

    protected function fetch()
    {
        if (!$this->api_key) {
            throw new \Exception('Missing forecast.io API key');
        }

        if (!$this->coordinates) {
            throw new \Exception('Missing coordinates');
        }

        $url = 'https://api.forecast.io/forecast/';
        $url .= $this->api_key . '/';
        $url .= implode(',', $this->coordinates);
        $url .= '?units=si&exclude=minutely,hourly,daily';

        $data = file_get_contents($url);

        if (!$data) {
            return false;
        }

        $data = json_decode($data, true);

        if (!isset($data['currently'])) {
            return false;
        }

        return $data;
    }

    protected function isDataExpired($data)
    {
        if (!isset($data['expires'])) {
            return true;
        }

        return ($data['expires'] - time()) < 0;
    }

    protected function cacheKey()
    {
        return '_v_weather_cache_' . md5(serialize($this->coordinates));
    }

    public static $_ = [];

    public static function build($name = '_global')
    {
        if (!isset(static::$_[$name])) {
            static::$_[$name] = new static;
        }

        return static::$_[$name];
    }
}

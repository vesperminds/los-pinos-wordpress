<?php defined('ABSPATH') or die;
/**
 * Plugin Name: Los Pinos
 * Description: Non-theme settings for Los Pinos
 * Author: Vesper Minds
 * Author URI: http://vesperminds.com/
 * Network: true
 */

// require_once __DIR__ . '/los-pinos/something.php';
require_once __DIR__ . '/los-pinos/reviews.php';
require_once __DIR__ . '/los-pinos/team_members.php';
require_once __DIR__ . '/los-pinos/rooms.php';
require_once __DIR__ . '/los-pinos/photos.php';
require_once __DIR__ . '/los-pinos/books.php';
require_once __DIR__ . '/los-pinos/weather.php';

V_Weather::build('monteverde')
->setApiKey('a03a2af76564e610c188b305155769ef')
->setCoordinates(10.3119843, -84.8171067)
->hydrate();

/** changing default wordpres email settings */

add_filter('wp_mail_from', function($old = '') {
	return 'no-reply@lospinos.net';
});

add_filter('wp_mail_from_name', function($old = '') {
	return 'Los Pinos';
});

/* Multi images */

add_filter('images_cpt', function() {
    return ['room'];
});

add_filter('list_images', function($list_images, $cpt) {
    global $typenow;

    $qty = 12;

    if ($typenow == 'room' || $cpt == 'room') {
        $qty = 18;
    }

    $picts = [];

    for ($i = 1; $i <= $qty; $i++) {
        $picts["image{$i}"] = "_image{$i}";
    }

    return $picts;
}, 10, 2);

/**
 * Wordpress footer
 */
add_filter('admin_footer_text', function($text) {
	return $text . ' <i>Potenciado por <a href="http://vesperminds.com/" target="_blank">Vesper Minds</a><i>';
});
